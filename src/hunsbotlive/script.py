import re
from datetime import timedelta

class Abort(Exception):
    pass


class IScript:
    def __init__(self, args):
        self.is_test = True if args.test else False
        self.tags = args.tags

    def __enter__(self, env=None):
        return self

    def __exit__(self, type, value, traceback, env=None):
        pass

    @property
    def name(self):
        raise NotImplemented

    @staticmethod
    def get_schedule(file):
        m = re.search(r'/scripts/(([0-9]+([a-z]+))+)/', file)
        if m:
            return m.group(1)
        else:
            return None

    @staticmethod
    def get_delay(schedule):
        if schedule == None:
            return timedelta(hours=1)

        parts = re.split(r'(d|h|min|s)', schedule)
        delta = timedelta()
        cur = 0
        for part in parts:
            if part.isdigit():
                cur = int(part)
            elif part == 'd':
                delta += timedelta(days=cur)
            elif part == 'h':
                delta += timedelta(hours=cur)
            elif part == 'min':
                delta += timedelta(minutes=cur)
            elif part == 's':
                delta += timedelta(seconds=cur)
            elif part == '':
                pass
            else:
                raise Exception(f"Unknown time specifier: {part}")
        return delta

    def edit(self, page):
        raise NotImplemented


    def save_changes(self, page, summary, tags=None):
        title = page.title()

        if self.is_test:
            print(f"Saving {title} (testmode): ”{summary}”")
        else:
            print(f"Saving {title}: ”{summary}”")
            page.save(summary, botflag=True, tags=tags, minor=False)


def test__get_schedule():
    schedule = IScript.get_schedule('/scripts/test.py')
    assert schedule == None
    schedule = IScript.get_schedule('/scripts/test/main.py')
    assert schedule == None
    schedule = IScript.get_schedule('/scripts/1d/test.py')
    assert schedule == '1d'
    schedule = IScript.get_schedule('/scripts/1h/test.py')
    assert schedule == '1h'
    schedule = IScript.get_schedule('/scripts/20min/test.py')
    assert schedule == '20min'
    schedule = IScript.get_schedule('/scripts/10s/test.py')
    assert schedule == '10s'
    schedule = IScript.get_schedule('/scripts/1h30min/test.py')
    assert schedule == '1h30min'


def test__get_delay():
    delay = IScript.get_delay('1d')
    assert delay == timedelta(days=1)
    delay = IScript.get_delay('1h')
    assert delay == timedelta(hours=1)
    delay = IScript.get_delay('20min')
    assert delay == timedelta(minutes=20)
    delay = IScript.get_delay('10s')
    assert delay == timedelta(seconds=10)
    delay = IScript.get_delay('1h30min15s')
    assert delay == timedelta(hours=1, minutes=30, seconds=15)
