from datetime import datetime
import logging
import json
import pytz

from .config import PATH

logger = logging.getLogger("__main__")

def make_state_file_name(state_id):
    return f'{PATH}/data/state.{state_id}.json'

def get_now():
    return datetime.utcnow().replace(microsecond=0, tzinfo=pytz.utc)


class State:
    def __init__(self, state_id=None):
        self.state_id = state_id
        if state_id:
            self.state_file = make_state_file_name(state_id)
            self.load()
        else:
            self.state = {}

    def set_end_time(self, time):
        self.end_time = time

    def load(self):
        try:
            with open(self.state_file, 'r') as fp:
                self.state = json.load(fp)
                logger.info(f"Loaded state {self.state} from {self.state_file}")
        except FileNotFoundError:
            self.state = {}
            logger.info(f"No existing state file: {self.state_file}")

    def save(self):
        if not self.state_id:
            return
        logger.info(f"Saving state: file: {self.state_file}")
        state = self.state
        state['time'] = self.end_time.isoformat().replace('+00:00', 'Z')
        state['updated'] = get_now().isoformat().replace('+00:00', 'Z')
        logger.info(f"State:", state)
        with open(self.state_file, 'w') as fp:
            json.dump(state, fp, indent=4)

    def json(self):
        return self.state
