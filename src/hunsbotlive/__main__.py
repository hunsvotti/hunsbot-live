#!/usr/bin/env python3

import argparse
import atexit
import collections
from datetime import datetime, timedelta
import importlib.util
import json
import logging
from logging.handlers import TimedRotatingFileHandler
import os
import signal
import sys
import time
import traceback

import pywikibot
from pywikibot import pagegenerators
from dateutil import parser as dateparser
import pytest
from pytz import timezone

from .script import Abort
from .config import LOGS_PATH, PATH, LOGDB_PATH, LIB_PATH, SITE
from .util.fancylog import FancyLog
from hunsbotlive.util.requested_titles import RequestedTitlesPageGenerator


from .state import State

sys.path.append(LIB_PATH)


logger = logging.getLogger(__name__)
htmllog = FancyLog(LOGDB_PATH)

def signal_handler(sig, frame):
    print('Exiting')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)




def setup_logging():
    FORMAT = '%(asctime)s:%(levelname)s:%(name)s: %(message)s'
    LOG_FILE = f'{LOGS_PATH}/hunsbot-live.log'

    formatter = logging.Formatter(FORMAT)

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(formatter)

    file_handler = TimedRotatingFileHandler(LOG_FILE,
                                            when='midnight',
                                            backupCount=10)
    file_handler.setFormatter(formatter)

    logger = logging.getLogger(__name__)
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)

    script_logger = logging.getLogger("scripts")
    script_logger.addHandler(stream_handler)
    script_logger.addHandler(file_handler)
    script_logger.setLevel(logging.DEBUG)



def parse_date(datestr):
    dt = dateparser.parse(datestr)
    if dt.tzinfo:
        return dt
    return dt.astimezone(timezone('Europe/Helsinki'))

def test__parse_date__parses_dates_with_timezone():
    dt = parse_date('2023-01-01 11:22:33Z')
    assert dt.tzinfo

def test__parse_date__assumes_helsinki_timezone():
    dt = parse_date('2023-01-01 11:22:33')
    assert dt.tzinfo

def test__parse_date__return_values_can_be_subtracted():
    dt1 = parse_date('2023-01-01 11:22:33')
    dt2 = parse_date('2023-01-01 11:22:33Z')
    assert str(dt2 - dt1) == '2:00:00'


def date_and_time(val):
    try:
        return parse_date(val)
    except:
        raise argparse.ArgumentTypeError("Invalid date value %s" % val)

def test__date_and_time__returns_datetime():
    assert type(date_and_time('2002-10-29')) == datetime

def test__date_and_time__parses_iso_dates():
    assert date_and_time('2002-10-29'), "Couldn't parse"
    assert date_and_time('2002-10-29 20:00:23'), "Couldn't parse"
    assert date_and_time('2002-10-29 20:00:23Z'), "Couldn't parse"

def test__date_and_time__sets_default_timezone_to_helsinki():
    assert date_and_time('2002-10-29 20:00:23').tzname() == "EET", "Wrong timezone"
    assert date_and_time('2002-10-29 20:00:23Z').tzname() == "UTC", "Wrong timezone"


def nonnegative_integer(val):
    if not val.isdigit():
        raise argparse.ArgumentTypeError('must be integer')

    intval = int(val)

    if intval < 0:
        raise argparse.ArgumentTypeError("Invalid value %s" % (val,))
    return intval

def test__nonnegative_integer__positive_values_are_ok():
    nonnegative_integer('1')
    nonnegative_integer('10')
    nonnegative_integer('100')

def test__nonnegative_integer__zero_is_ok():
    nonnegative_integer('0')

def test__nonnegative_integer__throws_with_negative_values():
    with pytest.raises(argparse.ArgumentTypeError):
        nonnegative_integer('-1'),

def test__nonnegative_integer__throws_with_non_integers():
    with pytest.raises(argparse.ArgumentTypeError):
        nonnegative_integer('a'),
    with pytest.raises(argparse.ArgumentTypeError):
        nonnegative_integer('0.2'),
    with pytest.raises(argparse.ArgumentTypeError):
        nonnegative_integer('1+2j'),

def path_to_module_name(path, root):
    if len(root) > 0:
        return path.replace(root + '/', '').replace('/', '.')
    return path.replace('/', '.')


def get_modules(root):
    rootdir = os.path.join(root, 'scripts')
    module_paths = {}
    for path, dirs, files in os.walk(rootdir):
        init_file = os.path.join(path, '__init__.py')
        dirs.sort() # sorts the dirs of each step before next step
        if os.path.isfile(init_file):
            module_name = path_to_module_name(path, root).removeprefix("scripts.")
            module_paths[module_name] = init_file
            continue

    return module_paths

def get_modules_by_schedule(root, schedules):
    module_paths = {}

    for schedule in schedules:
        rootdir = os.path.join(root, 'schedules', schedule)
        print(rootdir)
        for path, dirs, files in os.walk(rootdir, followlinks=True):
            print("PATH:", path)
            init_file = os.path.join(path, '__init__.py')
            dirs.sort() # sorts the dirs of each step before next step
            if os.path.isfile(init_file):
                module_name = path_to_module_name(path, root).removeprefix(f'schedules.{schedule}.')
                module_paths[module_name] = init_file
                continue

    return module_paths


def test__get_modules__returns_unscheduled_scripts():
    module_paths = get_modules(os.path.dirname(__file__))
    assert 'scripts.test' in module_paths, "Can't find test"

def test__get_modules__returns_scheduled_scripts():
    module_paths = get_modules(os.path.dirname(__file__))
    assert 'scripts.1h.test2' in module_paths, "Can't find test2"

def test__get_modules__returns_init_file_scripts():
    module_paths = get_modules(os.path.dirname(__file__))
    assert 'scripts.1h.edseur' in module_paths, "Can't find edseur"

def filter_modules_by_schedule(modules, schedules):
    out = {}

    for key, val in modules.items():
        parts = key.split('.')
        if len(parts) < 2:
            continue
        if parts[1] in schedules:
            out[key] = val

    return out


def test__filter_modules_by_schedule():
    modules = {
        'scripts.1h.edseur': 'path/to/edseur',
        'scripts.1h.toinen': 'path/to/toinen',
        'scripts.24h.katso': 'path/to/katso',
    }

    filtered = filter_modules_by_schedule(modules, '1h')

    assert len(filtered) > 0, "Empty result"
    assert len(filtered.keys()) == 2, "Incorrect number"
    assert filtered['scripts.1h.edseur'] == modules['scripts.1h.edseur']


def filter_modules_by_names(modules, names):
    out = {}

    for key, val in modules.items():
        for selector in names:
            if selector in key:
                out[key] = val

    return out

def test__filter_modules_by_names():
    modules = {
        'scripts.1h.edseur': 'path/to/edseur',
        'scripts.1h.toinen': 'path/to/toinen',
        'scripts.24h.katso': 'path/to/katso',
    }

    filtered = filter_modules_by_names(modules, ['toinen'])

    assert len(filtered.keys()) == 1, "Incorrect number"
    assert filtered['scripts.1h.toinen'] == modules['scripts.1h.toinen']

    filtered = filter_modules_by_names(modules, ['toinen', 'edseur'])

    assert len(filtered.keys()) == 2, "Incorrect number"
    assert filtered['scripts.1h.toinen'] == modules['scripts.1h.toinen']
    assert filtered['scripts.1h.edseur'] == modules['scripts.1h.edseur']

    filtered = filter_modules_by_names(modules, ['e'])

    assert len(filtered.keys()) == 2, "Incorrect number"
    assert filtered['scripts.1h.toinen'] == modules['scripts.1h.toinen']
    assert filtered['scripts.1h.edseur'] == modules['scripts.1h.edseur']


def import_module(module_name, module_path):
    spec = importlib.util.spec_from_file_location(module_name, module_path)
    mod = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = mod
    spec.loader.exec_module(mod)
    return mod


def test__import_module():
    mod = import_module('scripts.test', os.path.join(os.path.dirname(__file__), 'scripts', 'test', '__init__.py'))
    mod.ping()


def get_start_time(args, state):
    if args.start:
        return args.start

    if 'time' in state:
        return parse_date(state['time']) + timedelta(seconds=1)

    return None


def test__get_start_time__returns_start_argument_if_given():
    time_str = "2022-01-29 00:11:22"
    time = parse_date(time_str)
    class MockArgs:
        def __init__(self):
            self.start = parse_date(time_str)
    mockargs = MockArgs()

    result = get_start_time(mockargs, {})
    assert type(result) == datetime
    assert result == time


def test__get_start_time__returns_state_time_plus_1s_if_start_argument_not_given():
    time_str = "2011-01-29 00:11:22"
    class MockArgs:
        def __init__(self):
            self.start = None

    mockargs = MockArgs()
    mockstate = {
        'time': time_str
    }

    result = get_start_time(mockargs, mockstate)
    assert type(result) == datetime
    assert get_start_time(mockargs, mockstate).isoformat() == "2011-01-29T00:11:23+02:00"


def test__get_start_time__throws_if_invalid_time():
    time_str = "2011-01-29 00:11:e2"
    class MockArgs:
        def __init__(self):
            self.start = None

    mockargs = MockArgs()
    mockstate = {
        'time': time_str
    }

    with pytest.raises(ValueError):
        get_start_time(mockargs, mockstate)


def get_now():
    return datetime.utcnow().replace(microsecond=0).replace(tzinfo=timezone('UTC'))


def test__get_now__has_utc_timezone():
    now = get_now()
    assert now.tzname() == 'UTC'


def get_end_time(args, current_time: datetime):
    if (args.end != None and args.delay != None) or (args.end == None and args.delay == None):
        raise ValueError('Must give either delay or end')

    if args.end:
        return args.end

    if args.delay != None:
        delay_h = args.delay
    else:
        delay_h = 24

    return (current_time - timedelta(hours=delay_h)).replace(microsecond=0)


def test__get_end_time__returns_end_if_given():
    class MockArgs:
        def __init__(self):
            self.delay = None
            self.end = '2000-01-01 12:34:56'

    mockargs = MockArgs()

    end = get_end_time(mockargs, parse_date('3000-01-01 00:00:00'))
    assert end == '2000-01-01 12:34:56'

def test__get_end_time__returns_current_time_minus_delay():
    class MockArgs:
        def __init__(self):
            self.delay = 1
            self.end = None

    mockargs = MockArgs()

    end = get_end_time(mockargs, parse_date('2000-01-01 12:34:56'))
    assert end.isoformat() == '2000-01-01T11:34:56+02:00'


def test__get_end_time__throws_if_both_delay_and_end_is_given():
    class MockArgs:
        def __init__(self):
            self.delay = 3
            self.end = '2000-01-01 12:34:56'

    mockargs = MockArgs()

    with pytest.raises(ValueError):
        end = get_end_time(mockargs, parse_date('2000-01-01 12:34:56'))


def get_page_generator(options, state):
    site = pywikibot.Site(SITE[0], SITE[1])
    logger.info("Site: %s", site)

    if options.requests:
        logger.info("Handling articles from requests")
        return RequestedTitlesPageGenerator(site=site)

    if options.articles:
        logger.info("Handling articles from command line")
        return pagegenerators.PagesFromTitlesGenerator(
            options.articles,
            site=site
        )

    logger.info("Handling articles from recent changes")

    start_time = get_start_time(options, state)
    end_time = get_end_time(options, get_now())

    if not start_time:
        raise Exception("Couldn't determine start time. Did you mean to use --start or --articles or --requests?")
    if not end_time:
        raise Exception("Couldn't determine end time")

    logger.info("start time: %s" % (start_time,))
    logger.info("end time: %s" % (end_time,))

    return pagegenerators.RecentChangesPageGenerator(
        start = start_time,
        end = end_time,
        reverse = True,
        changetype = ['edit', 'new'],
        namespaces = options.namespaces or [0, 4],
        _filter_unique=filter_unique_pages,
        site=site
    )



def test__get_page_generator__returns_generator_for_articles_if_articles_arg_given():
    class MockArgs:
        def __init__(self):
            self.articles = ['bansku', 'omppu']

    mockargs = MockArgs()

    gen = get_page_generator(mockargs, {})
    for page in gen:
        assert page.title() in ['bansku', 'omppu']


def test__get_page_generator__returns_generator_for_recent_changes_if_delay_and_previous_time_given():
    class MockArgs:
        def __init__(self):
            self.articles = None
            self.start = None
            self.end = None
            self.delay = 1
            self.namespaces = None

    mockargs = MockArgs()
    mockstate = {
        'time': '2022-01-30 20:00:00Z'
    }

    gen = get_page_generator(mockargs, mockstate)
    assert type(gen) == type(pagegenerators.RecentChangesPageGenerator())


def test__get_page_generator__returns_generator_for_recent_changes_if_start_and_end_time_given():
    class MockArgs:
        def __init__(self):
            self.articles = None
            self.start = parse_date('2022-01-30 20:00:00')
            self.end = parse_date('2022-01-30 22:00:00')
            self.delay = None
            self.namespaces = None

    mockargs = MockArgs()
    mockstate = {
        'time': '2022-01-30 20:00:00Z'
    }

    gen = get_page_generator(mockargs, mockstate)
    assert type(gen) == type(pagegenerators.RecentChangesPageGenerator())


def test__get_page_generator__returns_generator_for_recent_changes_if_start_time_and_delay_given():
    class MockArgs:
        def __init__(self):
            self.articles = None
            self.start = parse_date('2022-01-30 20:00:00')
            self.end = None
            self.delay = 1
            self.namespaces = None

    mockargs = MockArgs()
    mockstate = {
        'time': '2022-01-30 20:00:00Z'
    }

    gen = get_page_generator(mockargs, mockstate)
    assert type(gen) == type(pagegenerators.RecentChangesPageGenerator())




def test__get_page_generator__throws_if_no_required_arguments_given():
    class MockArgs:
        def __init__(self):
            self.articles = None
            self.start = None
            self.end = None
            self.delay = None

    mockargs = MockArgs()
    mockstate = {
        'time': '2022-01-30'
    }

    with pytest.raises(ValueError):
        get_page_generator(mockargs, mockstate)


def filter_unique_pages(gen):
    seen = set()
    for page in gen:
        if page.title() in seen:
            continue
        seen.add(page.title())
        yield page
    return None



class ScriptContext:
    def __init__(self, scripts, env=None):
        self.scripts = scripts
        self.env = env

    def __enter__(self):
        for script in self.scripts:
            script.__enter__(self.env)
        return self.scripts

    def __exit__(self, type, value, traceback):
        for script in self.scripts:
            script.__exit__(type, value, traceback, self.env)


def apply_script_to_page(script, page, config):
    title = page.title()

    try:
        status = script.edit(page)
        logger.info(f"{script.name}: {title}: success: {status}")
        if type(status) == str:
            htmllog.success(script.name, title, status, config.test)
        elif status == None:
            htmllog.success(script.name, title, "None", config.test)
        else:
            for title, status in status.items():
                htmllog.success(script.name, title, status, config.test)
    except Abort as ab:
        logger.info(f"{script.name}: {title}: aborted: {ab}")
        htmllog.meeh(script.name, title, str(ab), config.test)
    except Exception as err:
        logger.error(f"{script.name}: {title}: error: {err}\n{traceback.format_exc()}")
        htmllog.error(script.name, title, str(err), config.test)
        if config.exit_on_error:
            raise err




if __name__ == "__main__":
    setup_logging()
    argparser = argparse.ArgumentParser(description='Updates see also links.')

    argparser.add_argument('--scripts', nargs='+',
                           help='Scripts to run. If not given, all will be run.')

    argparser.add_argument('--script-groups', nargs='+',
                           help='Script groups to run. If not given, all will be run.')

    argparser.add_argument('--state-id', '-i',
                           help='State file name. Will be stored in data dir.')

    argparser.add_argument('--start', '-s', type=date_and_time,
                           help='Datetime to start handling articles from. This is meant to be used only on manual '\
                           'runs. If not given, the end time (minus delay) of last run is used.')

    argparser.add_argument('--end', '-e', type=date_and_time,
                           help='Datetime to end handling articles to. This is meant to be used only on manual '\
                           'runs. ')

    argparser.add_argument('--delay', '-d', type=nonnegative_integer,
                           help='Delay in hours between edit and handling the edit. Current time minus delay will be the last '\
                           'time handled. Default is 24.')

    argparser.add_argument('--namespaces', '-n', nargs='+',
                           help='Namespaces.')

    argparser.add_argument('--tags', '--tag', '-t', nargs='+', help='Tag to give the edits.')

    argparser.add_argument('--articles', '-a', nargs='+', help='Article titles to handle. If given recent changes are not handled.')

    argparser.add_argument('--requests', '-r', action='store_true', help='Process requested titles.')

    argparser.add_argument('--test', '-T', action='store_true', help='Don’t actually save changes.')

    argparser.add_argument('--exit-on-error', '-X', action='store_true', help='Exit on script error.')

    args = argparser.parse_args()

    sys.path.append(os.path.dirname(__file__))
    module_files_by_name = get_modules(os.path.dirname(__file__))
    logger.info(f"Found scripts: {list(module_files_by_name.keys())}")

    if args.script_groups:
        #module_files_by_name = filter_modules_by_schedule(module_files_by_name, args.script_groups)
        module_files_by_name = get_modules_by_schedule(os.path.dirname(__file__), args.script_groups)
        print("module_files_by_name:", module_files_by_name)
    if args.scripts:
        module_files_by_name = filter_modules_by_names(module_files_by_name, args.scripts)

    if module_files_by_name == {}:
        sys.exit("No scripts to run")

    logger.info(f"Selected scripts: {list(module_files_by_name.keys())}")

    script_modules = [
        import_module(f'scripts.{name}', path) for name, path in module_files_by_name.items()
    ]

    scripts = [
        script_module.init(args) for script_module in script_modules if hasattr(script_module, 'init')
    ]

    state = State(args.state_id)
    if not args.test and not args.requests:
        state.set_end_time(get_end_time(args, get_now()))
        atexit.register(state.save)

    page_generator = get_page_generator(args, state.json())

    env = {
        'tags': args.tags,
        'test_mode': args.test,
        'exit_on_error': args.exit_on_error
    }

    with ScriptContext(scripts, env) as scripts:
        for page in page_generator:
            title = page.title()
            if not page.exists():
                logger.error(f"No such page: {title}")
                continue

            text = page.text
            logger.info(f"\n===== {title} =====")
            logger.info(f"Latest revision: {page.latest_revision.timestamp}")


            for script in scripts:
                with script:
                    apply_script_to_page(script, page, args)

                time.sleep(0.5)
