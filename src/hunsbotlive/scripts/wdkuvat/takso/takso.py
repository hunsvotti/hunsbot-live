import locale
import logging
import json
import re
import traceback

import pywikibot
from pywikibot.exceptions import NoPageError

from hunsbotlive.script import Abort
from hunsbotlive.util import unique, uppercasefirst
from ..util import format_image as format_image_base, has_image

logger = logging.getLogger(__name__)


def format_image(image_name, speciesname, taxon, gender=""):
    return format_image_base(image_name, f"{uppercasefirst(speciesname)}{gender} (''{taxon}'')")

site, repo, page, male_target, female_target = None, None, None, None, None


def get_wikidata_item(taxon):
    global site, repo, page, male_target, female_target
    if not site:
        site = pywikibot.Site("species", "species")
    if not repo:
        repo = site.data_repository()
    if not male_target:
        male_target = pywikibot.ItemPage(repo, "Q44148")
    if not female_target:
        female_target = pywikibot.ItemPage(repo, "Q43445")

    page = pywikibot.Page(site, taxon)


    try:
        item = pywikibot.ItemPage.fromPage(page)
    except pywikibot.exceptions.NoPageError:
        return None

    return item


def get_images(title, taxon, wditem):
    if "P18" not in wditem.claims:
        return []

    claim_list = wditem.claims["P18"]

    images = {}

    for claim in claim_list:
        img_title = claim.getTarget().title()
        if claim.has_qualifier("P21", male_target) and 'koiras' not in images:
            images['koiras'] = format_image(img_title, title, taxon, "koiras")
        elif claim.has_qualifier("P21", female_target) and 'naaras' not in images:
            images['naaras'] = format_image(img_title, title, taxon, "naaras")
        elif "P21" in claim.qualifiers:
            logger.info("muu sukuarvo")
        elif '' not in images:
            images[''] = format_image(img_title, title, taxon)

    if ('koiras' in images or 'naaras' in images) and '' in images:
        del images['']

    image_codes = []
    for gender, img_code in images.items():
        image_codes.append(img_code)
        return image_codes



def parse_takso(content):
        m = re.match(r"^takso\|([^|]+)$", content)
        if m:
            return m.group(1)
        m = re.match(r"^takso\|([^|]+)\|([^|]+)$", content)
        if m:
            if len(m.group(1)) == 1:
                return m.group(1) + m.group(2)
            else:
                return m.group(1) + " " + m.group(2)

        m = re.match(r"^takso\|([^|]+)\|([^|]+)\|([^|]+)$", content)
        if m:
            if len(m.group(1)) == 1:
                return m.group(1) + m.group(2) + " " + m.group(3)
            else:
                return m.group(1) + " " + m.group(2) + " " + m.group(3)



def get_taxons(wikitext):
    taxons = []
    for content in re.findall(r"\{\{(takso\|[^}]+)\}\}", wikitext):
        taxon = parse_takso(content)
        if taxon and taxon.find(' ') != -1 and taxon.find('*') == -1:
            taxons.append(taxon)
    return taxons

def process_taxons(title, taxons):
    images = []
    qnames = []

    for taxon in taxons:
        wditem = get_wikidata_item(taxon)

        if not wditem:
            logger.info(f"{title}: No such page in wikispecies: {taxon}")
            continue

        qname = wditem.title()
        imgs = get_images(title, taxon, wditem)

        qnames.append(qname)
        images += imgs

    logger.info(f"{title}: got images {', '.join(images)}")

    return qnames, images


def insert_images(wikitext, images):
    images_str = "\n".join(unique(images))
    wikitext1 = re.sub(r"(^|\n+)==( *)Suomi( *)==\n+===", r"\1==\2Suomi\3==\n" + images_str + r"\n\n===", wikitext, count=1)
    if wikitext1 != wikitext:
        return wikitext1

    return re.sub(r"(^|\n+)==( *)Suomi( *)==\n(.*?)\n+===", r"\1==\2Suomi\3==\n\4\n\n" + images_str + r"\n\n===", wikitext, count=1)


def save_changes(env, page, summary, tags=None):
    title = page.title()

    if env['test']:
        logger.info(f"{title}: Saving (testmode): ”{summary}”")

    else:
        logger.info(f"{title}: Saving: ”{summary}”")
        page.save(summary, botflag=True, tags=tags)


def handle_page(env, page):
    donedb = env['wdkuvatdb']

    title = page.title()

    wikitext = page.text

    original = wikitext

    suomios = get_suomios(wikitext)
    if not suomios:
        raise Abort("ei suomi-osiota")

    if has_image(suomios):
        raise Abort("on jo kuva")

    taxons = get_taxons(suomios)
    if len(taxons) == 0:
        raise Abort("Ei taksoneita")

    logger.info(f"{title}: found taxons {', '.join(taxons)}")

    qnames, images = process_taxons(title, taxons)

    if len(qnames) == 0:
        raise Abort(f"Ei wikispecies-sivuja taksoneille {taxons}")

    if len(images) == 0:
        raise Abort("Ei lisättäviä kuvia")

    logger.info(f"{title}: images: {', '.join(images)}")

    if len(images) > 0:
        wikitext = insert_images(wikitext, images)

    assert wikitext != original, "ei muuttunut"

    qlinks = [f'[[:wikidata:{qname}|{qname}]]' for qname in qnames]

    summary = '+ eliökuva Wikidatasta: %s' % (", ".join(qlinks),)

    page.text = wikitext.strip() + '\n'
    save_changes(env, page, summary, tags=env['tags'])
    if not env['test']:
        donedb[title] = str(page.editTime())
    logger.info(f"revision id: {page.latest_revision_id}")

    return qnames


def get_suomios(wikitext):
    suomi = re.search(r"(^|\n)== *Suomi *==\n", wikitext)
    if not suomi:
        return None

    start = suomi.start(0) + 10
    kans = re.search(r"(^|\n)== *Kansainvälinen *==\n", wikitext)
    muu = re.search(r"(^|\n)== *[^=\n]* *==\n", wikitext[start:])


    if muu:
        end = start + muu.start(0)
    else:
        end = len(wikitext)

    if kans:
        start = suomi.start(0)
    else:
        start = 0

    return wikitext[start:end]
