from hunsbotlive.util import uppercasefirst

def format_image(image_name, text):
    image_name = image_name.replace("File:", "").replace("Tiedosto:", "").replace("Image:", "").replace("Kuva:", "")
    return f"[[Kuva:{image_name}|pienoiskuva|{uppercasefirst(text)}]]"


def has_wikipedia_template(wikitext):
    if wikitext.find("{{Wikipedia}}") != -1 or wikitext.find("{{wikipedia}}") != -1 \
       or wikitext.find("{{Wikipedia|") != -1 or wikitext.find("{{wikipedia|") != -1:
        return True

    return False


def has_image(wikitext):
    if wikitext.find("[[Kuva:") != -1 or wikitext.find("[[Image:") != -1 \
       or wikitext.find("[[Tiedosto:") != -1 or wikitext.find("[[File:") != -1 \
    or wikitext.find("[[kuva:") != -1 or wikitext.find("[[image:") != -1 \
       or wikitext.find("[[tiedosto:") != -1 or wikitext.find("[[file:") != -1 \
    or wikitext.find("<gallery") != -1 \
    or wikitext.find("{{jaettu kuva/") != -1:
        return True

    return False
