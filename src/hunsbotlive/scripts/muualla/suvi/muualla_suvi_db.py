import sqlite3

class DBConn:

    def __init__(self, filename):
        self.filename = filename

    def __getitem__(self, title):
        cursor = self.conn.cursor()
        rows = cursor.execute('''
            SELECT a_id
            FROM muualla_suvi
            WHERE title = ?
            ''', (title,))

        return sorted(set(x[0] for x in rows.fetchall()), key=int)

    def __contains__(self, title):
        return len(self[title]) != 0

    def __enter__(self):
        self.conn = sqlite3.connect(self.filename)

        return self

    def __exit__(self, type, value, traceback):
        self.conn.close()
