from datetime import timedelta
import locale
import logging
import json
import os
import re
import traceback
import time

import pywikibot
import mwparserfromhell

from hunsbotlive.script import IScript, Abort

from hunsbotlive.util import has_template, is_redirect, is_ignorable, is_too_new, link_list, get_suomi_section_text
from .muualla_suvi_db import DBConn
from . import inserter

from hunsbotlive.config import PATH


logger = logging.getLogger(__name__)


locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


DB_FILE = os.path.join(os.path.dirname(__file__), 'muualla-suvi-data.sqlite3')


class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)
        if args.delay != None:
            self.min_delay = timedelta(seconds=args.delay)
        self.env = {
            'tags': self.tags,
            'test': self.is_test,
        }

    def __enter__(self, env=None):
        if 'muualla-suvi-db' not in self.env:
            self.env['muualla-suvi-db'] = DBConn(DB_FILE).__enter__()
        return self

    def __exit__(self, type, value, traceback, env=None):
        if 'muualla-suvi-db' in self.env:
            self.env['muualla-suvi-db'].__exit__(None, None, None)
            del self.env['muualla-suvi-db']

    @property
    def name(self):
        return __name__

    def edit(self, page):

        self.check_page_is_valid(page)
        template = self.get_template_text(page.title())
        self.add_template(page, template)
        self.save_changes(
            page,
            summary="Lisätty aiheesta muualla: Suomen viittomakielten verkkosanakirja (Suvi)",
            tags=self.tags
        )

        return page.title()

    def get_template_text(self, title):
        db = self.env['muualla-suvi-db']
        assert title in db, "ei tietokannassa"

        return "{{muualla-suvi|%s}}" % ("|".join(db[title]))

    def check_page_is_valid(self, page):
        title = page.title()
        db = self.env['muualla-suvi-db']

        if page.namespace() != 0:
            raise Abort("ei artikkelinimiavaruudessa")

        if title not in db:
            raise Abort("Ei tietokannassa")

        wikitext = page.text

        if is_redirect(wikitext):
            raise Abort("ohjaussivu")

        suomi_section = get_suomi_section_text(wikitext)
        if not suomi_section:
            raise Abort("ei suomi-osiota")

        if is_ignorable(suomi_section):
            raise Abort("kyseenalainen sivu")


        if has_template(suomi_section, 'muualla-suvi'):
            raise Abort("muualla-suvi-malline on jo")


    def add_template(self, page, template):
        wikitext = page.text
        new_wikitext = inserter.add_template_to_page(wikitext, template)

        if new_wikitext == wikitext:
            raise Abort("Ei muuttunut")

        page.text = new_wikitext





def init(args):
    return Script(args)
