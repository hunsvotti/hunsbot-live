from fiwikt.util import filter_words


class PosSectionScorer:
    def __init__(self, lang, index, rindex):
        self.lang = lang
        self.index = index
        self.rindex = rindex

    def score(self, pos_section_text):
        score = 0
        str_section = pos_section_text
        if ('taivutusmuoto' in str_section) or ('taivm' in str_section):
            score -= 10
        if '^===Aiheesta muualla===' in str_section:
            score += 10

        if self.rindex == -1:
            score += 5

        return score
