import sqlite3


class DBConn:

    def __init__(self, filename):
        self.filename = filename

    def __getitem__(self, title):
        cursor = self.conn.cursor()
        rows = cursor.execute('''
            SELECT title
            FROM muualla_ks
            WHERE title = ?
            ''', (title,))

        return rows.fetchone()

    def __contains__(self, title):
        return self[title] is not None

    def __enter__(self):
        self.conn = sqlite3.connect(self.filename)

        return self

    def __exit__(self, type, value, traceback):
        self.conn.close()
