import locale
import re
import sys

from fiwikt.section import SectionedPage, Section
import fiwikt.util.listparser as listparser

from wiki_section_parser import WikiPage, WikiSect
import wiki_section_parser.wikttitles as wikttitles

from .pos_section_scorer import PosSectionScorer

from hunsbotlive.script import Abort
from hunsbotlive.util import has_template


locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")



def score_sect(pos_sect, lang_code, index, rindex):
    scorer = PosSectionScorer(lang_code, index, rindex)

    return scorer.score(pos_sect.text)


def score_sects(pos_sects, lang_code):
    scores = [0] * len(pos_sects)

    for i, pos_sect in enumerate(pos_sects):
        scores[i] = score_sect(pos_sect, lang_code, i, i - len(pos_sects))

    return zip(scores, pos_sects)


def select_appropriate_pos_sect(pos_sects, lang_code):
    scored_sects = sorted(score_sects(pos_sects, lang_code), key=lambda x: -x[0])
    score, sect = scored_sects[0]

    return sect


# @param: text Page text
def add_template_to_page(text, template_text):
    wikicode = WikiPage(text)

    lang_sects = wikicode.root.subsections("Suomi")
    if len(lang_sects) == 0:
        raise Abort("Ei Suomi-osiota")

    pos_sects = lang_sects[0].subsections()
    selected_pos_sect = select_appropriate_pos_sect(pos_sects, 'fi')


    am_sects = selected_pos_sect.subsections('Aiheesta muualla')
    if len(am_sects) == 0:
        am_sects = [WikiSect('Aiheesta muualla', "")]
        selected_pos_sect.insert_with_ordermap(am_sects[0], wikttitles.titles)

    am_sects[0].lead = add_template(am_sects[0].lead or "", template_text)

    return wikicode.text


def add_template(text, template_text):
    if has_template(text, 'muualla-ks'):
        raise Abort("Ei lisättävää")

    parsed = listparser.parse_section(text)

    parsed.add(template_text)


    return str(parsed)
