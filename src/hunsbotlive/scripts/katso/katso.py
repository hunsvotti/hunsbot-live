from datetime import timedelta
import locale
import logging
import json
import re
import traceback

import pywikibot
from pywikibot import pagegenerators

from hunsbotlive.script import Abort

from hunsbotlive.util import is_redirect, is_ignorable, is_too_new, link_list


logger = logging.getLogger(__name__)



def save_changes(env, page, summary, tags=None):
    title = page.title()

    if env['test']:
        print(f"Saving {title} (testmode): ”{summary}”")
    else:
        print(f"Saving {title}: ”{summary}”")
        page.save(summary, botflag=True, tags=tags)


def handle_page(env, page):
    db = env['katsodb']

    title = page.title()
    wikitext = page.text

    original = wikitext

    other_titles = set(db[title])

    if len(other_titles) == 0:
        logger.info("%s: no katso pages", title)
        raise Abort("ei samankaltaisia sivuja")

    if wikitext.find("{{katso|") == -1:
        wikitext = re.sub(r"^\n*", "{{katso}}\n\n", wikitext)

    m = re.search(r'\{\{(katso\b[^}]*)\}\}', wikitext)
    assert m, "katso-mallinetta ei saatu lisättyä"

    old_pages = set( m.group(1).split('|')[1:] )

    combined_pages = old_pages.union(other_titles)
    new_pages = sorted(list(combined_pages.difference(old_pages)), key=locale.strxfrm)

    if len(new_pages) == 0:
        raise Abort("ei mitään lisättävää")

    combined_pages_ord = sorted(list(combined_pages), key=locale.strxfrm)

    wikitext = wikitext.replace(m.group(0), '{{katso|' + ('|'.join(combined_pages_ord)) + '}}')

    assert wikitext != original, "ei muuttunut"

    summary = 'katso: +%s' % (link_list(new_pages),)
    logger.info('%s', (combined_pages,))

    page.text = wikitext.strip() + '\n'
    save_changes(env, page, summary, tags=env['tags'])
    logger.info(f"revision id: {page.latest_revision_id}")

    return ", ".join(new_pages)



def get_filtered_pages(env, other_titles):
    db = env['katsodb']
    filtered_pages = []
    other_pages = pagegenerators.PagesFromTitlesGenerator(
                other_titles
            )
    for other_page in other_pages:
        other_title = other_page.title()
        if not other_page.exists():
            logger.info("Page doesn't exist anymore: %s", other_title)
            del db[other_title]
            continue

        other_wikitext = other_page.text
        if is_redirect(other_wikitext) or is_ignorable(other_wikitext):
            logger.info("Page is questionable or redirect: %s", other_title)
            del db[other_title]
            continue

        filtered_pages.append(other_page)

    return filtered_pages
