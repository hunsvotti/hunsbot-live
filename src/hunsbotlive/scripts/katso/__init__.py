from datetime import timedelta
import locale
import logging
import json
import re
import traceback
import time

import pywikibot
import mwparserfromhell

from hunsbotlive.script import IScript, Abort

from hunsbotlive.util import has_template, is_redirect, is_ignorable, is_too_new, link_list
from util.katsodb import DBConn

from . import katso
from .katso_key import katso_key
from config import PATH

logger = logging.getLogger(__name__)


locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


DB_FILE = '%s/data/katso-data.sqlite3' % (PATH,)


class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)
        if args.delay != None:
            self.min_delay = timedelta(seconds=args.delay)
        self.env = {
            'tags': self.tags,
            'test': args.test
        }

    def __enter__(self, env=None):
        if 'katsodb' not in self.env:
            self.env['katsodb'] = DBConn(DB_FILE).__enter__()
        return self

    def __exit__(self, type, value, traceback, env=None):
        if 'katsodb' in self.env:
            self.env['katsodb'].__exit__(None, None, None)
            del self.env['katsodb']
        pass

    @property
    def name(self):
        return __name__

    def edit(self, page):

        print("Title:", page.title())

        title = page.title()
        wikitext = page.text

        db = self.env['katsodb']

        if is_too_new(page, self.min_delay):
            raise Abort("sivu on liian uusi")

        if is_redirect(wikitext):
            raise Abort("ohjaussivu")

        if is_ignorable(wikitext):
            raise Abort("kyseenalainen sivu")


        if not db[title]:
            norm = katso_key(title)
            if norm == "":
                raise Abort("katso-avainta ei saatu")
            db[title] = norm


        other_titles = db[title]

        if len(other_titles) == 0:
            raise Abort("samankaltaisia sivunnimiä ei löytynyt")

        filtered_pages = katso.get_filtered_pages(self.env, other_titles)

        if len(filtered_pages) == 0:
            raise Abort("samankaltaisia sivunnimiä ei jäänyt")

        filtered_titles = [page.title() for page in filtered_pages]
        logger.info("%s: title set %s, filtered: %s", title, other_titles, filtered_titles)

        changed = {}
        for other_page in filtered_pages:
            logger.info("%s: Handling see also: %s", title, other_page.title())
            edits = katso.handle_page(self.env, other_page)
            changed[other_page.title()] = edits

        edits = katso.handle_page(self.env, page)
        changed[page.title()] = edits

        return changed



def init(args):
    return Script(args)
