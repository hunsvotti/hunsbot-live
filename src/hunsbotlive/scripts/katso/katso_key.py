import re
import unicodedata

unstroked = {
    "Ⱥ": "A",
    "ⱥ": "A",
    "Ƀ": "B",
    "ƀ": "B",
    "Ꞓ": "C",
    "ꞓ": "C",
    "Ȼ": "C",
    "ȼ": "C",
    "Đ": "D",
    "đ": "D",
    "Ɖ": "D",
    "ɖ": "D",
    "Ꟈ": "D",
    "ꟈ": "D",
    "Ɇ": "E",
    "ɇ": "E",
    "ꬳ": "E",
    "Ꞙ": "F",
    "ꞙ": "F",
    "Ꞡ": "G",
    "ꞡ": "G",
    "Ǥ": "G",
    "ǥ": "G",
    "Ħ": "H",
    "ħ": "H",
    "Ɨ": "I",
    " ɨ": "I",
    "Ɉ": "J",
    "ɉ": "J",
    "Ꝃ": "K",
    "ꝃ": "K",
    "Ꞣ": "K",
    "ꞣ": "K",
    "Ꝁ": "K",
    "ꝁ": "K",
    "Ꝅ": "K",
    "ꝅ": "K",
    "Ƚ": "L",
    "ƚ": "L",
    "Ⱡ": "L",
    "ⱡ": "L",
    "Ꝉ": "L",
    "ꝉ": "L",
    "Ł": "L",
    "ł": "L",
    "ᴌ": "L",
    "Ꞥ": "N",
    "ꞥ": "N",
    "Ɵ": "O",
    "Ꝋ": "O",
    "ꝋ": "O",
    "Ø": "O",
    "ø": "O",
    "Ᵽ": "P",
    "ᵽ": "P",
    "Ꝑ": "P",
    "ꝑ": "P",
    "Ꝙ": "Q",
    "ꝙ": "Q",
    "Ꝗ": "Q",
    "ꝗ": "Q",
    "Ꞧ": "R",
    "ꞧ": "R",
    "Ɍ": "R",
    "ɍ": "R",
    "Ꞩ": "S",
    "ꞩ": "S",
    "Ꟊ": "S",
    "ꟊ": "S",
    "Ⱦ": "T",
    "ⱦ": "T",
    "Ŧ": "T",
    "ŧ": "T",
    "Ʉ": "U",
    "ʉ": "U",
    "Ꞹ": "U",
    "ꞹ": "U",
    "Ꝟ": "V",
    "ꝟ": "V",
    "Ɏ": "Y",
    "ɏ": "Y",
    "Ƶ": "Z",
    "ƶ": "Z",
}


def katso_key(title):
    # erikoiskäsittelyt
    title = title.replace("ß", "zxzxzx")

    norm = unicodedata.normalize("NFD", title)
    key  = re.sub("\W", "", norm).upper()
    key  = re.sub("[- _]", "", key)

    for letter in unstroked.keys():
        key = key.replace(letter, unstroked[letter])


    # erikoiskäsittelyjen palautus
    key = key.replace("zxzxzx", "ß")

    return key
