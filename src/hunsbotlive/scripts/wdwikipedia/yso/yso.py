import locale
import logging
import json
import re
import traceback

import pywikibot
from pywikibot import pagegenerators
from pywikibot.exceptions import NoPageError

from hunsbotlive.script import Abort

from util import unique, uppercasefirst, is_all_conjugation

logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


# P2347 = YSO id
QUERY = """
SELECT ?item ?itemLabel
WHERE
{
  ?item wdt:P2347 "%s".
}
"""

def yso_query(id):
    id = str(id)
    if id[0] == 'p':
        id = id[1:]
    assert id.isnumeric(), "Virheellinen id"

    return QUERY % (id,)

def format_wikipedia_template(fiwikt_title, fiwiki_title):
    if uppercasefirst(fiwikt_title) == fiwiki_title:
        return "{{Wikipedia}}"
    else:
        #return "{{Wikipedia|%s}}" % (fiwiki_title,)
        raise Abort("Wikipedia-sivu on eriniminen")


site, repo, page = None, None, None


def get_wikidata_item(taxon):
    global site, repo, page, male_target, female_target
    if not site:
        site = pywikibot.Site("species", "species")
    if not repo:
        repo = site.data_repository()

    page = pywikibot.Page(site, taxon)

    try:
        item = pywikibot.ItemPage.fromPage(page)
    except pywikibot.exceptions.NoPageError:
        return None

    return item


def get_wikipedialink(title, wditem):

    try:
        fiwiki_title = wditem.getSitelink('fiwiki')
        return format_wikipedia_template(title, fiwiki_title)
    except NoPageError:
        return None



def handle_yso_item(title, ysoitem):
    logger.info(f"{title}: ysoitem {ysoitem['prefLabel']}:{ysoitem['localname']}")
    yso_id = ysoitem['localname']

    query = yso_query(yso_id)

    wikidata_site = pywikibot.Site("wikidata", "wikidata")
    generator = pagegenerators.WikidataSPARQLPageGenerator(query, site=wikidata_site)

    qnames = []
    wikitemplates = []
    for wditem in generator:
        qname = wditem.title()
        fiwiki_template = get_wikipedialink(title, wditem)
        if fiwiki_template is not None:
            qnames.append(wditem.title())
            wikitemplates.append(fiwiki_template)
    return qnames, wikitemplates


def save_changes(env, page, summary, tags=None):
    title = page.title()

    if env['test']:
        logger.info(f"{title}: Saving (testmode): ”{summary}”")
    else:
        logger.info(f"{title}: Saving: ”{summary}”")
        page.save(summary, botflag=True, tags=tags)


def insert_wikipedia_templates(wikitext, templates):
    wikis_str = "\n".join(templates)
    wikitext = re.sub(r"(^|\n+)(==[^=])", '\n' + wikis_str + r"\n\n\2", wikitext, count=1)
    wikitext = wikitext.lstrip()
    return wikitext


def handle_page(env, page, ysoitems):
    donedb = env['wdwikipediadb']

    title = page.title()
    wikitext = page.text

    original = wikitext
    fiwikis = []
    qnames = []

    for ysoitem in ysoitems:
        qs, w = handle_yso_item(title, ysoitem)
        if len(qs) > 0:
            qnames += qs
            fiwikis += w

    if len(qnames) == 0:
        raise Abort("Wikidata-kohteita ei löytynyt")

    logger.info(f"{title}: got wikidata items {', '.join(qnames)}")

    if len(fiwikis) == 0:
        raise Abort("Ei lisättäviä wikilinkkejä")

    logger.info(f"{title}: got wikilinks: {', '.join(fiwikis)}")

    wikitext = insert_wikipedia_templates(wikitext, fiwikis)

    assert wikitext != original, "ei muuttunut"

    qlinks = [f'[[:wikidata:{qname}|{qname}]]' for qname in qnames]

    summary = '+ Wikipedia-linkki Wikidatasta: %s' % (", ".join(qlinks),)

    page.text = wikitext.strip() + '\n'
    save_changes(env, page, summary, tags=env['tags'])
    if not env['test']:
        donedb[title] = str(page.editTime())
    logger.info(f"revision id: {page.latest_revision_id}")

    return qnames



def get_suomios(wikitext):
    suomi = re.search(r"(^|\n)== *Suomi *==\n", wikitext)
    if not suomi:
        return None

    start = suomi.start(0) + 10
    kans = re.search(r"(^|\n)== *Kansainvälinen *==\n", wikitext)
    muu = re.search(r"(^|\n)== *[^=\n]* *==\n", wikitext[start:])


    if muu:
        end = start + muu.start(0)
    else:
        end = len(wikitext)

    if kans:
        start = suomi.start(0)
    else:
        start = 0

    return wikitext[start:end]
