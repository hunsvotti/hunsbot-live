def has_wikipedia_template(wikitext):
    if wikitext.find("{{Wikipedia}}") != -1 or wikitext.find("{{wikipedia}}") != -1 \
       or wikitext.find("{{Wikipedia|") != -1 or wikitext.find("{{wikipedia|") != -1:
        return True

    return False
