from datetime import timedelta
import locale
import logging
import json
import re
import traceback
import time

import pywikibot
import mwparserfromhell

from hunsbotlive.script import IScript, Abort

from hunsbotlive.util import has_template, is_redirect, is_ignorable, is_too_new, link_list
from ..util import has_wikipedia_template
from util.donedb import DBConn

from . import takso
from config import PATH

logger = logging.getLogger(__name__)


locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


DB_FILE = '%s/data/wdwikipedia-data.sqlite3' % (PATH,)


class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)
        if args.delay != None:
            self.min_delay = timedelta(seconds=args.delay)
        self.env = {
            'tags': self.tags,
            'test': self.is_test,
        }

    def __enter__(self, env=None):
        if 'wdwikipediadb' not in self.env:
            self.env['wdwikipediadb'] = DBConn("wdwikipedia", DB_FILE).__enter__()
        return self

    def __exit__(self, type, value, traceback, env=None):
        if 'wdwikipediadb' in self.env:
            self.env['wdwikipediadb'].__exit__(None, None, None)
            del self.env['wdwikipediadb']

    @property
    def name(self):
        return __name__

    def edit(self, page):
        donedb = self.env['wdwikipediadb']

        title = page.title()

        if page.namespace() != 0:
            raise Abort("ei artikkelinimiavaruudessa")

        if title in donedb:
            raise Abort("tehdyissä")

        wikitext = page.text

        if is_redirect(wikitext):
            raise Abort("ohjaussivu")

        suomios = takso.get_suomios(wikitext)
        if not suomios:
            raise Abort("ei suomi-osiota")

        if is_ignorable(suomios):
            raise Abort("kyseenalainen sivu")

        if suomios.find("{{takso|") == -1:
            raise Abort("ei takso-mallinetta")

        lead = wikitext[0:wikitext.find("==")]
        if has_wikipedia_template(suomios) or has_wikipedia_template(lead):
            raise Abort("Wikipedia-linkki on jo")

        qlinks = takso.handle_page(self.env, page)

        return ", ".join(qlinks)





def init(args):
    return Script(args)
