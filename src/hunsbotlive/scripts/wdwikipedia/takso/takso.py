import locale
import logging
import json
import re
import traceback

import pywikibot
from pywikibot.exceptions import NoPageError

from hunsbotlive.script import Abort
from util import unique, uppercasefirst


logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")

def info(env):
    return {
        'groups': ["delay-" + str(DELAY_IN_HOURS) + "h"],
    }

def init(env):
    if 'donedb' not in env:
        env['wdwikipediadb'] = DBConn("wdwikipedia", DB_FILE).__enter__()

def exit(env):
    if 'wdwikipediadb' in env:
        env['wdwikipediadb'].__exit__(None, None, None)
        del env['wdwikipediadb']

def uppercasefirst(s):
    return s[0].upper() + s[1:]


def format_wikipedia_template(fiwikt_title, fiwiki_title):
    if uppercasefirst(fiwikt_title) == fiwiki_title:
        return "{{Wikipedia}}"
    else:
        return "{{Wikipedia|%s}}" % (fiwiki_title,)


site, repo, page = None, None, None


def get_wikidata_item(taxon):
    global site, repo, page, male_target, female_target
    if not site:
        site = pywikibot.Site("species", "species")
    if not repo:
        repo = site.data_repository()

    page = pywikibot.Page(site, taxon)

    try:
        item = pywikibot.ItemPage.fromPage(page)
    except pywikibot.exceptions.NoPageError:
        return None

    return item


def get_wikipedialink(title, wditem):

    try:
        fiwiki_title = wditem.getSitelink('fiwiki')
        fiwiki_template = format_wikipedia_template(title, fiwiki_title)
    except NoPageError:
        fiwiki_template = None

    return fiwiki_template


def parse_takso(content):
        m = re.match(r"^takso\|([^|]+)$", content)
        if m:
            return m.group(1)
        m = re.match(r"^takso\|([^|]+)\|([^|]+)$", content)
        if m:
            if len(m.group(1)) == 1:
                return m.group(1) + m.group(2)
            else:
                return m.group(1) + " " + m.group(2)

        m = re.match(r"^takso\|([^|]+)\|([^|]+)\|([^|]+)$", content)
        if m:
            if len(m.group(1)) == 1:
                return m.group(1) + m.group(2) + " " + m.group(3)
            else:
                return m.group(1) + " " + m.group(2) + " " + m.group(3)


def has_wikipedia_template(wikitext):
    if wikitext.find("{{Wikipedia}}") != -1 or wikitext.find("{{wikipedia}}") != -1 \
       or wikitext.find("{{Wikipedia|") != -1 or wikitext.find("{{wikipedia|") != -1:
        return True

    return False


def get_taxons(wikitext):
    taxons = []
    for content in re.findall(r"\{\{(takso\|[^}]+)\}\}", wikitext):
        taxon = parse_takso(content)
        if taxon and taxon.find(' ') != -1 and taxon.find('*') == -1:
            taxons.append(taxon)
    return taxons

def process_taxons(title, taxons):
    fiwikis = []
    qnames = []

    for taxon in taxons:
        wditem = get_wikidata_item(taxon)

        if not wditem:
            logger.info(f"{title}: No such page in wikispecies: {taxon}")
            continue

        qname = wditem.title()
        fiwiki_template = get_wikipedialink(title, wditem)

        qnames.append(qname)
        if fiwiki_template:
            fiwikis.append(fiwiki_template)

    fiwikis = unique(fiwikis)
    logger.info(f"{title}: got wikilinks {', '.join(fiwikis)}")
    return qnames, fiwikis


def insert_wikipedia_templates(wikitext, templates):
    wikis_str = "\n".join(templates)
    wikitext = re.sub(r"(^|\n+)(==[^=])", '\n' + wikis_str + r"\n\n\2", wikitext, count=1)
    wikitext = wikitext.lstrip()
    return wikitext


def save_changes(env, page, summary, tags=None):
    title = page.title()

    if env['test']:
        logger.info(f"{title}: Saving (testmode): ”{summary}”")
    else:
        logger.info(f"{title}: Saving: ”{summary}”")
        page.save(summary, botflag=True, tags=tags)


def handle_page(env, page):
    donedb = env['wdwikipediadb']

    title = page.title()
    wikitext = page.text

    original = wikitext

    suomios = get_suomios(wikitext)
    assert suomios, "no suomi section"

    taxons = get_taxons(suomios)
    if len(taxons) == 0:
        raise Abort("Ei taksoneita")

    logger.info(f"{title}: found taxons {', '.join(taxons)}")

    qnames, fiwikis = process_taxons(title, taxons)
    if len(qnames) == 0:
        raise Abort(f"Ei wikispecies-sivuja taksoneille {taxons}")

    assert len(qnames) > 0, "Ei q nameja"

    if len(fiwikis) == 0:
        raise Abort("Ei lisättäviä wikilinkkejä")

    logger.info(f"{title}: got wikilinks: {', '.join(fiwikis)}")

    wikitext = insert_wikipedia_templates(wikitext, fiwikis)

    assert wikitext != original, "ei muuttunut"

    qlinks = [f'[[:wikidata:{qname}|{qname}]]' for qname in qnames]

    summary = '+ Wikipedia-linkki Wikidatasta: %s' % (", ".join(qlinks),)

    page.text = wikitext.strip() + '\n'
    save_changes(env, page, summary, tags=env['tags'])
    if not env['test']:
        donedb[title] = str(page.editTime())
    logger.info(f"revision id: {page.latest_revision_id}")

    return qnames




def get_suomios(wikitext):
    suomi = re.search(r"(^|\n)== *Suomi *==\n", wikitext)
    if not suomi:
        return None

    start = suomi.start(0) + 10
    kans = re.search(r"(^|\n)== *Kansainvälinen *==\n", wikitext)
    muu = re.search(r"(^|\n)== *[^=\n]* *==\n", wikitext[start:])


    if muu:
        end = start + muu.start(0)
    else:
        end = len(wikitext)

    if kans:
        start = suomi.start(0)
    else:
        start = 0

    return wikitext[start:end]



def edit(env, page):
    donedb = env['wdwikipediadb']

    title = page.title()
    logger.info("Edit: %s", title)

    if page.namespace() != 0:
        raise Abort("ei artikkelinimiavaruudessa")

    if title in donedb:
        raise Abort("tehdyissä")

    wikitext = page.text

    if is_redirect(wikitext):
        raise Abort("ohjaussivu")

    suomios = get_suomios(wikitext)
    if not suomios:
        raise Abort("ei suomi-osiota")

    if is_ignorable(suomios):
        raise Abort("kyseenalainen sivu")

    if suomios.find("{{takso|") == -1:
        raise Abort("ei takso-mallinetta")

    lead = wikitext[0:wikitext.find("==")]
    if has_wikipedia_template(suomios) or has_wikipedia_template(lead):
        raise Abort("Wikipedia-linkki on jo")

    qlinks = handle_page(env, page)

    return ", ".join(qlinks)
