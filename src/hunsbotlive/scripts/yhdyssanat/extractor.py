import re
import sys

import mwparserfromhell

LIBPATH = '/home/antti/.local/lib/python3.8/omat'
if LIBPATH not in sys.path:
    sys.path.append(LIBPATH)

import fiwikt.langdata as langdata


base_pos_title_by_code = {
    None: "*",
    "a": "Adjektiivi",
    "adv": "Adverbi",
    "n": "Nomini",
    "s": "Substantiivi",
    "v": "Verbi",
}


def parse_ety_template(template):
    assert template.name == "ety", "Not ety template"
    lang = str(template.get("1").value)

    if template.get("2") == "yhdyssana" and template.get("5", None) == None:
        return lang, '*', [str(template.get("3").value), str(template.get("4").value)]

    return None, None, []


def parse_yhdyssana_template(template):
    assert template.name == "yhdyssana", "Not yhdyssana template"
    kieli_p = template.get("kieli", None)
    k_p = template.get("k", None)
    lang = str(k_p.value) if k_p else str(kieli_p.value) if kieli_p else None

    base_pos_code = template.get("kanta-sanalk", None)
    base_pos_code = str(base_pos_code.value) if base_pos_code else None
    base_pos = base_pos_title_by_code[base_pos_code]

    parts = get_positional_parameters(template)
    parts = [x for x in filter(lambda x: not x.startswith('-') and not x.endswith('-'), parts)]

    if len(parts) > 0:
        return lang, base_pos, parts

    return None, None, []


def parse_yhdyssana_taiv_template(template):
    assert template.name == "yhdyssana-taiv", "Not yhdyssana-taiv template"
    kieli_p = template.get("kieli", None)
    k_p = template.get("k", None)
    lang = str(k_p.value) if k_p else str(kieli_p.value) if kieli_p else None

    base_pos = None

    if not template.get("5", None) == None:
        return None, None, []

    parts = [str(template.get("2").value), str(template.get("3").value)]

    if len(parts) > 0:
        return lang, base_pos, parts

    return None, None, []


def get_positional_parameters(template):
    i = 1
    vals = []
    while True:
        p = template.get(str(i), None)
        if not p:
            break
        vals.append(str(p.value))
        i += 1

    return vals


def parse_template(template):
    if template.name == "ety":
        return parse_ety_template(template)
    if template.name == "yhdyssana-taiv":
        return parse_yhdyssana_taiv_template(template)
    if template.name == "yhdyssana":
        return parse_yhdyssana_template(template)

    return None, None, []




def get_language_name_for_code(lang_code):
    return langdata.nom_for_code(lang_code)


def language_code_and_heading_match(lang_code, lang_heading):
    if not lang_code:
        return True

    lang_name = get_language_name_for_code(str(lang_code))

    if not lang_name:
        return False

    if lang_name == lang_heading.lower():
        return True

    return False


def gen_compound_etys_on_page_text(text):
    wikicode = mwparserfromhell.parse(text)

    for langsection in wikicode.get_sections(levels=[2], include_lead=False, include_headings=True):
        heading = langsection.filter_headings()[0]
        language_heading = str(heading.title)
        for possection in langsection.get_sections(levels=[3], include_lead=False, include_headings=True):
            pos_heading = possection.filter_headings()[0]
            pos_heading = str(pos_heading.title)
            for etysection in possection.get_sections(levels=[4], include_lead=False, include_headings=False, matches="Etymologia"):
                # Varmistetaan, että yhdyssanamalline on ensimmäinen asia osiossa, ja osio joko loppuu siihen tai sen jälkeen tulee
                # virkkeen lopettava merkki.
                m = re.match(r"^\n\*? *\{\{[^}]+\}\} *(<|←|$|;|\.|\n)", str(etysection))
                if not m:
                    continue
                template_text = mwparserfromhell.parse(m.group(0))
                for template in template_text.filter_templates():
                    lang_code, base_pos, words = parse_template(template)
                    if lang_code is None or language_code_and_heading_match(lang_code, str(language_heading)):
                        if len(words) > 0:
                            yield language_heading, base_pos, pos_heading, words

    return None, None, None


def get_compounds_on_page(page):
    return gen_compound_etys_on_page_text(page.text)


def get_compounds_by_language_by_pos_by_title(page):
    inverted = {}
    for language, base_pos, compound_pos, words in get_compounds_on_page(page):
        title = page.title()
        print(words, ">", language, ">", base_pos, ">", compound_pos, ">", title)
        for word in words:
            if word not in inverted:
                inverted[word] = {}
            if language not in inverted[word]:
                inverted[word][language] = {}
            if base_pos not in inverted[word][language]:
                inverted[word][language][base_pos] = {}
            if compound_pos not in inverted[word][language][base_pos]:
                inverted[word][language][base_pos][compound_pos] = []
            if title not in inverted[word][language][base_pos][compound_pos]:
                inverted[word][language][base_pos][compound_pos].append(title)

    return inverted
