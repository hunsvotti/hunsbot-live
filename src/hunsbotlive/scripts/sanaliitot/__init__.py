from datetime import timedelta
import locale
import logging
import traceback

import pywikibot
import pywikibot.pagegenerators as pagegenerators

from hunsbotlive.script import IScript, Abort

from hunsbotlive.util import has_template, is_redirect, is_ignorable, is_too_new, link_list
from . import extractor as extractor
from . import inserter as inserter

from fiwikt.section import SectionedPage, Section
import fiwikt.langdata as lang_data


logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")

from config import PATH, SITE


class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)
        if args.delay != None:
            self.min_delay = timedelta(seconds=args.delay)

    def __enter__(self, env=None):
        return self

    def __exit__(self, type, value, traceback, env=None):
        pass

    @property
    def name(self):
        return __name__

    def edit(self, page):
        print("Title:", page.title())

        title = page.title()
        wikitext = page.text

        if page.namespace() != 0:
            raise Abort("ei artikkelinimiavaruudessa")

        if is_redirect(wikitext):
            raise Abort("ohjaussivu")

        if is_ignorable(wikitext):
            raise Abort("kyseenalainen sivu")

        return self.handle_main_page(page)


    def handle_main_page(self, page):
        words_by_language_by_title = extractor.get_set_phrases_by_language_by_pos_by_title(page)

        if words_by_language_by_title == {}:
            raise Abort("sanaliittoja ei löytynyt")

        pages = pagegenerators.PagesFromTitlesGenerator(
            words_by_language_by_title.keys(),
            site=pywikibot.Site(SITE[0], SITE[1])
        )

        dones = {}
        for page in pages:
            words_by_language = words_by_language_by_title[page.title()]
            done = None
            if page.exists():
                done = self.handle_page(page, words_by_language)
            if done:
                dones[page.title()] = done

        if dones == {}:
            raise Abort("Ei mitään lisättävää")

        return dones


    def handle_page(self, page, words_by_language):
        title = page.title()
        wikitext = page.text
        original = wikitext

        new_wikitext, added = inserter.add_words_to_page(wikitext, words_by_language)

        if len(added) == 0:
            return None

        summary = f"Lisätty {'sanaliitto' if len(added) == 1 else 'sanaliitot'} {link_list(added)}"

        page.text = new_wikitext

        self.save_changes(page, summary, tags=self.tags)

        return (", ".join(added))


    def save_changes(self, page, summary, tags=None):
        title = page.title()

        if self.is_test:
            print(f"Saving {title} (testmode): ”{summary}”")
        else:
            print(f"Saving {title}: ”{summary}”")
            page.save(summary, botflag=True, tags=tags, minor=False)


def init(args):
    return Script(args)
