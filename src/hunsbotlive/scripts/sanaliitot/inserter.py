import locale
import re
import sys

LIBPATH = '/home/antti/.local/lib/python3.8/omat'
if LIBPATH not in sys.path:
    sys.path.append(LIBPATH)

from fiwikt.section import SectionedPage, Section
from fiwikt.linkremover import remove_links
from fiwikt.util import filter_words
import fiwikt.yhdyssanatsection.parser as yhdyssanatparser

import fiwikt.langdata as lang_data

from wiki_section_parser import WikiPage, WikiSect
import wiki_section_parser.wikttitles as wikttitles

from .pos_section_scorer import PosSectionScorer

from hunsbotlive.script import Abort


locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


def has_word(text, word):
    return text.find("[[" + word + "]]") != -1 \
        or text.find("|" + word + "|") != -1 \
        or text.find("|" + word + "}}") != -1


def score_sect(pos_sect, lang_code, words):
    scorer = PosSectionScorer(lang_code, words)

    return scorer.score(pos_sect.text)


def score_sects(pos_sects, lang_code, words):
    scores = [0] * len(pos_sects)

    for i, pos_sect in enumerate(pos_sects):
        scores[i] = score_sect(pos_sect, lang_code, words)

    return zip(scores, pos_sects)


def select_appropriate_pos_sect(pos_sects, lang_code, words_by_derivpos):
    # Combine lists all deriv pos'es
    all_words = []
    for derivpos, words in words_by_derivpos.items():
        all_words += words

    scored_sects = sorted(score_sects(pos_sects, lang_code, all_words), key=lambda x: -x[0])
    score, sect = scored_sects[0]

    return sect


# @param: text Page text
# @param: words_by_language_by_pos looks like this
#   {'Language': {'Base word POS': {'Derived word POS': [List of words]}}}
# for example: {'Suomi': {'*': {'Substantiivi': ['rikkikiisu']}}}, where * means unknown base pos
def add_words_to_page(text, words_by_language_by_pos):
    wikicode = WikiPage(text)

    added = []
    for lang, words_by_basepos_by_derivpos in words_by_language_by_pos.items():
        lang_sects = wikicode.root.subsections(lang)
        lang_code = lang_data.code_for_nom(lang.lower())
        if len(lang_sects) == 0:
            continue

        for base_pos, words_by_derivpos in words_by_basepos_by_derivpos.items():
            if base_pos == '*':
                pos_sects = lang_sects[0].subsections()
            else:
                pos_sects = lang_sects[0].subsections(base_pos)

            if len(pos_sects) == 0:
                continue

            selected_pos_sect = select_appropriate_pos_sect(pos_sects, lang_code, words_by_derivpos)

            ls_sects = selected_pos_sect.subsections('Liittyvät sanat')

            if len(ls_sects) == 0:
                ls_sects = [WikiSect("Liittyvät sanat", "")]
                selected_pos_sect.insert_with_ordermap(ls_sects[0], wikttitles.titles)

            ys_sects = ls_sects[0].subsections('Sanaliitot')
            if len(ys_sects) == 0:
                ys_sects = ls_sects[0].subsections('Yhdyssanat ja sanaliitot')

            if len(ys_sects) == 0:
                ys_sects = [WikiSect("Sanaliitot", "")]
                ls_sects[0].insert_with_ordermap(ys_sects[0], wikttitles.titles)

            for pos, words in words_by_derivpos.items():
                ys_sects[0].lead, added_words = add_words(ys_sects[0].lead or "", words, False, lang_code)
                added += added_words
            ls_sects[0].lead = remove_links(ls_sects[0].lead or "", added_words)

    return wikicode.text, added


def add_words(text, words, force_botti_lisäsi, lang_code):
    if (lang_code and lang_code != "fi") and text.strip() == "" and not force_botti_lisäsi:
        text = "\n{{yhdyssanat|%s}}\n\n" % (lang_code,)

    words = [x for x in filter(lambda word: not has_word(text, word), words)]
    if len(words) == 0:
        raise Abort("Ei lisättävää")

    parsed = yhdyssanatparser.parse_section(text, force_botti_lisäsi, lang_code)

    added = parsed.add(words)


    return str(parsed), added


def test__add_words__to_existing_template():
    orig = """
{{yhdyssanat|fi|kalamato|matokala}}
"""

    result = add_words(orig, ["kalainen vesi", "vesinen kala"], False, "fi")
    print("***********************************")
    print(result)
    print("***********************************")

    expected = """
{{yhdyssanat|fi|kalamato|kalavesi|matokala|vesikala}}
"""

    assert result.strip() == expected.strip()


def test__add_words__to_non_existing_template():
    orig = """
"""

    result = add_words(orig, ["kalainen vesi", "vesinen kala"], False, "fi")
    print("***********************************")
    print(result)
    print("***********************************")

    expected = """
{{yhdyssanat|fi|kalavesi|vesikala}}
"""

    assert result.strip() == expected.strip()
