from datetime import timedelta
import locale
import logging
import json
import re
import traceback

import pywikibot
from pywikibot import pagegenerators

from hunsbotlive.script import Abort

DELAY_IN_HOURS = 24
logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


def info(env):
    return {
        'groups': ["delay-" + str(DELAY_IN_HOURS) + "h"],
    }

def remove_link(title, wikitext):
    out = wikitext

    out = re.sub(r' *([–-]) *\n\[\[%s\]\] *[–-] *(?=\n)' % (title,), r' \1\n', out)
    out = re.sub(r' *\n([–-]) *\[\[%s\]\] *\n[–-] *' % (title,), r'\n\1 ', out)

    out = re.sub(r'\n\n\[\[%s\]\] *\n[–-] *' % (title,), r'\n', out)
    out = re.sub(r'\n\n\[\[%s\]\] *\n(?=\n)' % (title,), r'\n', out)

    out = re.sub(r'= *\n\[\[%s\]\] *\n[–-] *' % (title,), r'=\n', out)
    out = re.sub(r'= *\n\[\[%s\]\] *\n(?=\n)' % (title,), r'=\n', out)
    out = re.sub(r'= *\n\[\[%s\]\] *\n=' % (title,), r'=\n\n=', out)
    out = re.sub(r'= *\n\[\[%s\]\][ \n]*$' % (title,), r'=\n\n', out)

    out = re.sub(r' *([–-]) *\[\[%s\]\] *[–-] *' % (title,), r' \1 ', out)

    out = re.sub(r'\n\[\[%s\]\] *[–-] *' % (title,), ' ', out)
    out = re.sub(r' *[–-] *\[\[%s\]\] *\n' % (title,), ' ', out)

    out = re.sub(r'^\* *\[\[%s\]\] *\n' % (title,), '\n', out)
    out = re.sub(r'^\* *\[\[%s\]\] *$' % (title,), '\n', out)
    out = re.sub(r'\n\* *\[\[%s\]\] *(?=\n)' % (title,), '\n', out)
    out = re.sub(r'\n\* *\[\[%s\]\] *$' % (title,), '\n', out)
    out = re.sub(r' [–-]\n\[\[%s\]\] *[\n$]' % (title,), '\n', out)

    return out


def save_changes(env, page, summary, tags=None):
    title = page.title()

    if env['test']:
        print(f"Saving {title} (testmode): ”{summary}”")
    else:
        print(f"Saving {title}: ”{summary}”")
        page.save(summary, botflag=True, tags=tags)


def handle_index_page(env, page):
    db = env['toiveetdb']

    title = page.title()
    wikitext = page.text

    pattern = re.compile(r'\[\[(.*?)\]\]')
    titles_now = []
    for listed_title in re.findall(pattern, wikitext):
        bar = listed_title.find("|")
        if bar > -1:
            listed_title = listed_title[:bar - 1]

        if re.match("^[^:]+:", listed_title):
            continue
        titles_now.append(listed_title)

    titles_before = db.pages_of_index(title)
    titles_now = set(titles_now)
    titles_before = set(titles_before)

    added = titles_now - titles_before
    removed = titles_before - titles_now

    db.clear_index(title)

    for listed_title in titles_now:
        db[listed_title] = title

    return "päivitettiin (+%s, –%s)" % (list(added), list(removed))


def get_language_for_index_page(index_page):
    index_page_name = index_page.title()

    # Poikkeus
    if index_page_name.startswith("Wikisanakirja:Suomen kielen sanalistan puuttuvat sivut/"):
        lang_heading = "Suomi"

    parts = index_page_name.split("/")
    if len(parts) < 2:
        raise Exception("kieltä ei saatu sivun nimestä: %s", index_page_name)

    lang_heading = parts[1]
    lang_heading = re.match(r"^\D+", lang_heading).group(0)
    lang_heading = lang_heading.strip()


    return lang_heading


def page_contains_language(env, page, lang_heading):
    if not page.exists():
        logger.info(f"sivua ei ole enää: {page.title()}")
        return False


    article_wikitext = page.text
    article_wikitext = "\n" + article_wikitext

    if article_wikitext.find("\n==%s==\n" % (lang_heading,)) == -1:
        print("Kieltä ei sivulla")
        return False

    logger.info("kieli %s on artikkelisivulla" % lang_heading)

    return True


def remove_request_from_index_page(env, index_page, article_page):
    db = env['toiveetdb']
    article_title = article_page.title()

    if not index_page.exists():
        logger.info(f"{index_page.title()}: hakemisto sivua ei ole enää")
        return False

    index_wikitext = index_page.text

    index_wikitext_edited = remove_link(article_title, index_wikitext)
    if index_wikitext_edited == index_wikitext:
        logger.info(f"{index_page.title()}: linkkiä ei saatu poistettua ({article_title})")
        return False

    db.delete(index_page.title(), article_title)

    summary = '[[%s]] ✔' % (article_title,)

    index_page.text = index_wikitext_edited.strip() + '\n'

    save_changes(env, index_page, summary, tags=env['tags'])
    logger.info(f"revision id: {index_page.latest_revision_id}")

    return True


def process_index_pages(env, index_page_names, article_page):
    index_pages = pagegenerators.PagesFromTitlesGenerator(
        index_page_names
    )

    saved = []
    for index_page in index_pages:
        lang = get_language_for_index_page(index_page)
        if page_contains_language(env, article_page, lang):
            if remove_request_from_index_page(env, index_page, article_page):
                saved.append(index_page.title())

    return saved

def handle_article_page(env, article_page):
    db = env['toiveetdb']
    title = article_page.title()

    index_page_names = db[title]

    if len(index_page_names) == 0:
        raise Abort("sivua ei ole indeksoiduissa artikkelitoivelistoissa")

    saved = process_index_pages(env, index_page_names, article_page)

    if len(saved) == 0:
        raise Abort("oli hakemistossa, muttei artikkelitoivesivulla (poistettu käsin?)")

    return str(saved)


def is_article_request_page(page):
    namespace = page.namespace()
    title = page.title()
    if namespace == 4 \
       and (
           title.startswith('Wikisanakirja:Artikkelitoiveet')
           or title.startswith('Wikisanakirja:Suomen kielen sanalistan puuttuvat sivut') \
       ):
        return True
    return False

def is_too_new(page):
    repo = pywikibot.Site().data_repository()
    if page.editTime() > repo.server_time() - timedelta(hours=DELAY_IN_HOURS):
        return True
    return False


def edit(env, page):
    title = page.title()
    namespace = page.namespace()

    if is_too_new(page):
        raise Abort("sivu on liian uusi")

    if is_article_request_page(page):
        return handle_index_page(env, page)

    if namespace != 0:
        raise Abort("ei artikkelinimiavaruudessa")

    return handle_article_page(env, page)
