from datetime import timedelta
import locale
import logging
import json
import re
import traceback
import time

import pywikibot
import mwparserfromhell

from hunsbotlive.script import IScript, Abort

from hunsbotlive.util import has_template, is_redirect, is_ignorable, is_too_new, link_list
from util.toiveetdb import DBConn

from . import toiveet
from config import PATH

logger = logging.getLogger(__name__)


locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")


DB_FILE = '%s/data/artikkelitoiveet-data.sqlite3' % (PATH,)


class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)
        if args.delay != None:
            self.min_delay = timedelta(seconds=args.delay)
        self.env = {
            'tags': self.tags,
            'test': args.test
        }

    def __enter__(self, env=None):
        if 'toiveetdb' not in self.env:
            self.env['toiveetdb'] = DBConn(DB_FILE).__enter__()
        return self

    def __exit__(self, type, value, traceback, env=None):
        if 'toiveetdb' in self.env:
            self.env['toiveetdb'].__exit__(None, None, None)
            del self.env['toiveetdb']
        pass

    @property
    def name(self):
        return __name__

    def edit(self, page):
        title = page.title()
        namespace = page.namespace()

        if is_too_new(page, self.min_delay):
            raise Abort("sivu on liian uusi")

        if toiveet.is_article_request_page(page):
            return toiveet.handle_index_page(self.env, page)

        if namespace != 0:
            raise Abort("ei artikkelinimiavaruudessa")

        return toiveet.handle_article_page(self.env, page)




def init(args):
    return Script(args)
