from datetime import timedelta
import locale
import logging
import json
import re
import traceback
import time

import pywikibot
import pywikibot.pagegenerators as pagegenerators
import mwparserfromhell

from hunsbotlive.script import IScript, Abort

from hunsbotlive.util import has_template, is_redirect, is_ignorable, is_too_new, link_list
from . import extractor
from . import inserter
from config import SITE


logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")




class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)
        if args.delay != None:
            self.min_delay = timedelta(seconds=args.delay)

    def __enter__(self, env=None):
        return self

    def __exit__(self, type, value, traceback, env=None):
        pass

    @property
    def name(self):
        return __name__

    def edit(self, page):
        print("Title:", page.title())

        title = page.title()
        wikitext = page.text

        if is_too_new(page, self.min_delay):
            raise Abort(f"sivu on liian uusi: delay {self.min_delay}")

        if is_redirect(wikitext):
            raise Abort("ohjaussivu")

        if is_ignorable(wikitext):
            raise Abort("kyseenalainen sivu")

        return self.handle_main_page(page)

    def handle_main_page(self, page):
        title = page.title()

        words_by_language_by_pos_by_title = extractor.get_derivatives_by_language_by_pos_by_title(page)

        if words_by_language_by_pos_by_title == {}:
            raise Abort("johdoksia ei löytynyt")

        other_titles = words_by_language_by_pos_by_title.keys()
        pages_gen = pagegenerators.PagesFromTitlesGenerator(
            filter(lambda title: not title.startswith('-') and not title.endswith('-'), other_titles),
            site=pywikibot.Site(SITE[0], SITE[1])
        )

        dones = {}

        for page in pages_gen:
            words_by_language_by_pos = words_by_language_by_pos_by_title[page.title()]
            added = self.handle_page(page, words_by_language_by_pos)
            if added:
                dones[page.title()] = added

        if dones == {}:
            raise Abort("Ei mitään lisättävää")

        return dones


    def handle_page(self, page, words_by_language_by_pos):
        wikitext = page.text
        original = wikitext

        wikitext, added = inserter.add_words_to_page(wikitext, words_by_language_by_pos)

        if len(added) == 0:
            return []

        if wikitext == original:
            return []

        page.text = wikitext

        summary = f"Lisätty {'johdos' if len(added) == 1 else 'johdokset'} {link_list(added)}"

        self.save_changes(page, summary, self.tags)

        return (", ".join(added))


    def save_changes(self, page, summary, tags=None):
        title = page.title()

        if self.is_test:
            print(f"Saving {title} (testmode): ”{summary}”")
        else:
            print(f"Saving {title}: ”{summary}”")
            page.save(summary, botflag=True, tags=tags, minor=False)


def init(args):
    return Script(args)
