from fiwikt.util import filter_words


class PosSectionScorer:
    def __init__(self, lang, words):
        self.lang = lang
        self.words = words

    def score(self, pos_section_text):
        score = 0
        str_section = pos_section_text
        if ('taivutusmuoto' in str_section) or ('taivm' in str_section):
            score -= 10
        if '^===Aiheesta muualla===' in str_section:
            score -= 10
        if '^===Lähteet===' in str_section:
            score -= 10
        if '^===Viitteet===' in str_section:
            score -= 10

        if '===Substantiivi===' in str_section:
            if self.lang == "en":
                score += 100
            else:
                score += 10
        if '===Adjektiivi===' in str_section:
            score += 5

        if '=====Yhdyssanat=====' in str_section:
            score += 15
        elif '=====Yhdyssanat ja sanaliitot=====' in str_section:
            score += 15

        # Note: There's only one word almost always
        # If the word exists on one of the sections, then it is likely
        # the correct one.
        if len(filter_words(str_section, self.words)) < len(self.words):
            score += 100

        return score
