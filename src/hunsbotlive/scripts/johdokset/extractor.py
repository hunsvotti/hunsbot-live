import re
import sys

import mwparserfromhell

LIBPATH = '/home/antti/.local/lib/python3.8/omat'
if LIBPATH not in sys.path:
    sys.path.append(LIBPATH)

import fiwikt.langdata as langdata


base_pos_title_by_code = {
    None: "*",
    "a": "Adjektiivi",
    "adv": "Adverbi",
    "n": "Nomini",
    "s": "Substantiivi",
    "v": "Verbi",
}


def get_param_value(template, param_name):
    p = template.get(param_name, None)
    if p:
        return str(p.value)
    return None


def parse_ety_template(template):
    assert template.name == "ety", "Not ety template"
    lang = str(template.get("1").value)

    if template.get("2") == "johdos" and get_param_value(template, "5") == None:
        return lang, '*', [str(template.get("3").value), str(template.get("4").value)]

    if template.get("1") == "fi" and get_param_value(template, "3") == 'lainen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]
    if template.get("1") == "fi" and get_param_value(template, "3") == 'läinen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]
    if template.get("1") == "fi" and get_param_value(template, "3") == 'ilainen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]
    if template.get("1") == "fi" and get_param_value(template, "3") == 'iläinen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]

    if template.get("1") == "fi" and get_param_value(template, "4") == 'lainen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("4").value)]
    if template.get("1") == "fi" and get_param_value(template, "4") == 'läinen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("4").value)]
    if template.get("1") == "fi" and get_param_value(template, "4") == 'ilainen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("4").value)]
    if template.get("1") == "fi" and get_param_value(template, "4") == 'iläinen':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("4").value)]

    if template.get("1") == "fi" and get_param_value(template, "4") == 'us':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]
    if template.get("1") == "fi" and get_param_value(template, "4") == 'ys':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]

    if template.get("1") == "fi" and get_param_value(template, "4") == 'uus':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]
    if template.get("1") == "fi" and get_param_value(template, "4") == 'yys':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("3").value)]

    if template.get("1") == "fi" and get_param_value(template, "4") == 'sti':
        return lang, '*', [str(template.get("2").value), '-' + str(template.get("4").value)]

    return None, None, []

def parse_fi_johdos_2_template(template):
    assert template.name == "fi-johdos/2" or template.name == "johdos2", "Not fi-johdos/2 template"
    if template.name == "johdos2":
        k = get_param_value(template, 'k')
        assert k == None or k == 'fi'
    lang = 'fi'

    base_pos_code = get_param_value(template, "s")
    if base_pos_code:
        base_pos = base_pos_title_by_code[base_pos_code]
    else:
        base_pos = "*"

    if get_param_value(template, "4") == None:
        return lang, base_pos, [str(template.get("1").value), '-' + str(template.get("3").value)]

    return None, None, []

def parse_fi_johdos_3_template(template):
    assert template.name == "fi-johdos/3" or template.name == "johdos3", "Not fi-johdos/3 template"
    if template.name == "johdos3":
        assert str(get_param_value(template, 'k')) == 'fi'
    lang = 'fi'

    base_pos_code = get_param_value(template, "s")
    if base_pos_code:
        base_pos = base_pos_title_by_code[base_pos_code]
    else:
        base_pos = "*"

    if get_param_value(template, "5") == None:
        return lang, base_pos, [str(template.get("1").value), '-' + str(template.get("3").value)]

    return None, None, []

def parse_johdos_template(template):
    assert template.name == "johdos", "Not johdos template"
    kieli_p = template.get("kieli", None)
    k_p = template.get("k", None)
    lang = str(k_p.value) if k_p else str(kieli_p.value) if kieli_p else None

    base_pos_code = template.get("kanta-sanalk", None)
    base_pos_code = str(base_pos_code.value) if base_pos_code else None
    base_pos = base_pos_title_by_code[base_pos_code]

    parts = get_positional_parameters(template)
    parts = [x for x in filter(lambda x: not x.startswith('-') and not x.endswith('-'), parts)]

    if len(parts) > 0:
        return lang, base_pos, parts

    return None, None, []

def get_positional_parameters(template):
    i = 1
    vals = []
    while True:
        p = template.get(str(i), None)
        if not p:
            break
        vals.append(str(p.value))
        i += 1

    return vals

def parse_template(template):
    if template.name == "ety":
        return parse_ety_template(template)
    if template.name == "fi-johdos/2":
        return parse_fi_johdos_2_template(template)
    if template.name == "fi-johdos/3":
        return parse_fi_johdos_3_template(template)
    if template.name == "johdos2":
        return parse_fi_johdos_2_template(template)
    if template.name == "johdos/3":
        return parse_fi_johdos_3_template(template)
    if template.name == "johdos":
        return parse_johdos_template(template)

    return None, None, []




def get_language_name_for_code(lang_code):
    return langdata.nom_for_code(lang_code)


def language_code_and_heading_match(lang_code, lang_heading):
    if not lang_code:
        return True

    lang_name = get_language_name_for_code(str(lang_code))

    if not lang_name:
        return False

    if lang_name == lang_heading.lower():
        return True

    return False


def gen_derivative_etys_on_page_text(text):
    wikicode = mwparserfromhell.parse(text)

    for langsection in wikicode.get_sections(levels=[2], include_lead=False, include_headings=True):
        heading = langsection.filter_headings()[0]
        language_heading = str(heading.title)
        for possection in langsection.get_sections(levels=[3], include_lead=False, include_headings=True):
            pos_heading = possection.filter_headings()[0]
            pos_heading = str(pos_heading.title)
            for etysection in possection.get_sections(levels=[4], include_lead=False, include_headings=False, matches="Etymologia"):
                # Varmistetaan, että yhdyssanamalline on ensimmäinen asia osiossa, ja osio joko loppuu siihen tai sen jälkeen tulee
                # virkkeen lopettava merkki.
                m = re.match(r"^\n\*? *\{\{[^}]+\}\} *(<|←|$|;|\.|\n)", str(etysection))
                if not m:
                    continue
                template_text = mwparserfromhell.parse(m.group(0))
                for template in template_text.filter_templates():
                    lang_code, base_pos, words = parse_template(template)
                    if lang_code is None or language_code_and_heading_match(lang_code, str(language_heading)):
                        if len(words) > 0:
                            yield language_heading, base_pos, pos_heading, words

    return None, None, None


def get_derivatives_on_page(page):
    return gen_derivative_etys_on_page_text(page.text)


def get_derivatives_by_language_by_pos_by_title(page):
    inverted = {}
    for language, base_pos, deriv_pos, words in get_derivatives_on_page(page):
        title = page.title()
        print(words, ">", language, ">", base_pos, ">", deriv_pos, ">", title)
        for word in words:
            if word not in inverted:
                inverted[word] = {}
            if language not in inverted[word]:
                inverted[word][language] = {}
            if base_pos not in inverted[word][language]:
                inverted[word][language][base_pos] = {}
            if deriv_pos not in inverted[word][language][base_pos]:
                inverted[word][language][base_pos][deriv_pos] = []
            if title not in inverted[word][language][base_pos][deriv_pos]:
                inverted[word][language][base_pos][deriv_pos].append(title)

    return inverted
