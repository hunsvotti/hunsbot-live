import locale
import logging
import json
import re
import traceback
import time

import pywikibot

from hunsbotlive.script import IScript, Abort

from .logic import clone_edellinen_seuraava_template
from .util import has_template, is_redirect, is_ignorable, is_too_new

logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "fi_FI.utf-8")




class Script(IScript):

    def __init__(self, args=None):
        super().__init__(args)
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)

    def __enter__(self, env=None):
        return self

    def __exit__(self, type, value, traceback, env=None):
        pass

    @property
    def name(self):
        return __name__

    def edit(self, page):
        print("Title:", page.title())

        title = page.title()
        wikitext = page.text

        if is_too_new(page, self.min_delay):
            raise Abort("sivu on liian uusi")

        if is_redirect(wikitext):
            raise Abort("ohjaussivu")

        if is_ignorable(wikitext):
            raise Abort("kyseenalainen sivu")

        self.handle_page(page)

        return None

    def handle_page(self, page):
        title = page.title()
        wikitext = page.text
        original = wikitext

        wikitext = re.sub(r'((edellinen|seuraava) *= *\[\[[^]]+)\]\]', r'\1/]]', wikitext)
        try:
            wikitext = clone_edellinen_seuraava_template(wikitext)
        except Exception as e:
            raise Abort(e)

        if wikitext == original:
            raise Abort("Ei muuttunut")

        page.text = wikitext

        summary = "Lisätty alaboksi"

        self.save_changes(page, summary, self.tags)


    def save_changes(self, page, summary, tags=None):
        title = page.title()

        if self.is_test:
            print(f"Saving {title} (testmode): ”{summary}”")
        else:
            print(f"Saving {title}: ”{summary}”")
            page.save(summary, botflag=True, tags=tags)


def init(args):
    return Script(args)
