import re

import mwparserfromhell
import pytest


def get_ed_seur_from_otsikko_template(wikicode):
    for template in wikicode.filter_templates():
        if template.name.strip() == "Otsikko":
            ed = None
            if template.has("edellinen"):
                ed = template.get("edellinen").value.strip()

            seur = None
            if template.has("seuraava"):
                seur = template.get("seuraava").value.strip()

            return ed, seur

    return None, None

def test__get_ed_seur_from_otsikko_template__returns_edellinen_and_seuraava():
    wikitext = """{{Otsikko|edellinen=a|seuraava=b}}"""
    wikicode = mwparserfromhell.parse(wikitext)
    ed, seur = get_ed_seur_from_otsikko_template(wikicode)
    assert ed == 'a', 'Incorrect edellinen'
    assert seur == 'b', 'Incorrect seuraava'

def test__get_ed_seur_from_otsikko_template__returns_none_none_if_no_otsikko():
    wikitext = """==Otsikko puuttuu=="""
    wikicode = mwparserfromhell.parse(wikitext)
    ed, seur = get_ed_seur_from_otsikko_template(wikicode)
    assert ed == None, 'Incorrect edellinen'
    assert seur == None, 'Incorrect seuraava'

def test__get_ed_seur_from_otsikko_template__returns_none_none_if_no_edellinen_and_seuraava_params():
    wikitext = """{{Otsikko}}"""
    wikicode = mwparserfromhell.parse(wikitext)
    ed, seur = get_ed_seur_from_otsikko_template(wikicode)
    assert ed == None, 'Incorrect edellinen'
    assert seur == None, 'Incorrect seuraava'


def get_ed_seur_from_alaboksi_template(wikicode):
    for template in wikicode.filter_templates():
        if template.name.strip() == "Alaboksi":
            ed = None
            if template.has("edellinen"):
                ed = template.get("edellinen").value.strip()

            seur = None
            if template.has("seuraava"):
                seur = template.get("seuraava").value.strip()

            return ed, seur

    return None, None

def test__get_ed_seur_from_alaboksi_template__returns_edellinen_and_seuraava():
    wikitext = """{{Alaboksi|edellinen=a|seuraava=b}}"""
    wikicode = mwparserfromhell.parse(wikitext)
    ed, seur = get_ed_seur_from_alaboksi_template(wikicode)
    assert ed == 'a', 'Incorrect edellinen'
    assert seur == 'b', 'Incorrect seuraava'

def test__get_ed_seur_from_alaboksi_template__returns_none_none_if_no_alaboksi():
    wikitext = """==Alaboksi puuttuu=="""
    wikicode = mwparserfromhell.parse(wikitext)
    ed, seur = get_ed_seur_from_alaboksi_template(wikicode)
    assert ed == None, 'Incorrect edellinen'
    assert seur == None, 'Incorrect seuraava'

def test__get_ed_seur_from_alaboksi_template__returns_none_none_if_no_edellinen_and_seuraava_params():
    wikitext = """{{Alaboksi}}"""
    wikicode = mwparserfromhell.parse(wikitext)
    ed, seur = get_ed_seur_from_alaboksi_template(wikicode)
    assert ed == None, 'Incorrect edellinen'
    assert seur == None, 'Incorrect seuraava'



def add_before_categories(wikicode, wikitext_to_insert):
    wikitext = str(wikicode)

    m = re.search(r"(^|\n+)((\[\[(Luokka|\w{2,3}(-\w+){0,2}):[^\[\]<>]+\]\]\s*)*)$", wikitext)
    if not m:
        raise Exception("Can't find insertion position")

    end_of_prev = m.start(0)
    start_of_cats = m.start(2)

    text = wikitext[:end_of_prev] + wikitext_to_insert + wikitext[start_of_cats:]

    return text

def test__add_before_categories__adds_before_luokka_links():
    wikitext = """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.

[[Luokka:Testi1]]
[[Luokka:Testi2]]
"""
    wikicode = mwparserfromhell.parse(wikitext)
    new_text = add_before_categories(wikicode, "\n\n{{Alaboksi}}\n\n")
    print()
    print('********************')
    print(new_text)
    print('********************')
    print()
    assert new_text != wikicode, "Did not change"
    assert new_text == """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.

{{Alaboksi}}

[[Luokka:Testi1]]
[[Luokka:Testi2]]
""", "Does not match"

def test__add_before_categories__adds_before_last_luokka_links():
    wikitext = """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.

[[Luokka:Testi3]]

Some more content.

[[Luokka:Testi1]]
[[Luokka:Testi2]]
"""
    wikicode = mwparserfromhell.parse(wikitext)
    new_text = add_before_categories(wikicode, "\n\n{{Alaboksi}}\n\n")

    assert new_text != wikicode, "Did not change"
    assert new_text == """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.

[[Luokka:Testi3]]

Some more content.

{{Alaboksi}}

[[Luokka:Testi1]]
[[Luokka:Testi2]]
""", "Does not match"


def test__add_before_categories__adds_before_last_language_links():
    wikitext = """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.

[[Luokka:Testi3]]

Some more content.

[[en:Testi1]]
[[Luokka:Testi2]]
"""
    wikicode = mwparserfromhell.parse(wikitext)
    new_text = add_before_categories(wikicode, "\n\n{{Alaboksi}}\n\n")

    assert new_text != wikicode, "Did not change"
    assert new_text == """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.

[[Luokka:Testi3]]

Some more content.

{{Alaboksi}}

[[en:Testi1]]
[[Luokka:Testi2]]
""", "Does not match"


def test__add_before_categories__adds_at_end_if_no_cats_or_lang_links():
    wikitext = """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.
"""
    wikicode = mwparserfromhell.parse(wikitext)
    new_text = add_before_categories(wikicode, "\n{{Alaboksi}}\n\n")

    print(new_text)

    assert new_text != wikicode, "Did not change"
    assert new_text == """
{{Otsikko|edellinen=a|seuraava=b}}

Some content.
{{Alaboksi}}

""", "Does not match"



def has_alaboksi_template(wikitext):
    wikicode = mwparserfromhell.parse(wikitext)
    for template in wikicode.filter_templates():
        if template.name.strip() == "Alaboksi":
            return True
    return False

def test__has_alaboksi_template__finds_existing_alaboksi():
    wikitext = """
{{Alaboksi
|edellinen=Sivu 1
|seuraava=Sivu 2
}}
"""
    assert has_alaboksi_template(wikitext), "Didn't find alaboksi"

    wikitext = """{{Alaboksi|edellinen=Sivu 1|seuraava=Sivu 2}}"""

    assert has_alaboksi_template(wikitext), "Didn't find alaboksi"

    wikitext = """
{{Alaboksi
 | edellinen=Sivu 1
 | seuraava=Sivu 2
}}
"""
    assert has_alaboksi_template(wikitext), "Didn't find alaboksi"

def test__has_alaboksi_template__doesnt_find_non_existing_alaboksi():
    wikitext = """{{Sivuboksi}}"""
    assert not has_alaboksi_template(wikitext), "Found alaboksi, but shouldn't"

    wikitext = """{{AlaboksinJatke}}"""
    assert not has_alaboksi_template(wikitext), "Found alaboksi, but shouldn't"

    wikitext = """{{PääAlaboksi}}"""
    assert not has_alaboksi_template(wikitext), "Found alaboksi, but shouldn't"

    wikitext = """{{{Alaboksi}}}"""
    assert not has_alaboksi_template(wikitext), "Found alaboksi, but shouldn't"

    wikitext = """Alaboksi"""
    assert not has_alaboksi_template(wikitext), "Found alaboksi, but shouldn't"


def update_alaboksi(wikitext, ed, seur):
    wikicode = mwparserfromhell.parse(wikitext)

    for template in wikicode.filter_templates():
        if template.name.strip() == "Alaboksi":
            template['edellinen'] = ed
            template['seuraava'] = seur

    return wikicode


def test__update_alaboksi__updates_content():
    text = """{{Alaboksi|edellinen=pois|seuraava=pois}}"""
    new_text = update_alaboksi(text, "EDELLINEN", "SEURAAVA")

    assert new_text == "{{Alaboksi|edellinen=EDELLINEN|seuraava=SEURAAVA}}", "Didn't update"


def test__update_alaboksi__unsets_content():
    text = """{{Alaboksi|edellinen=pois|seuraava=pois}}"""
    new_text = update_alaboksi(text, None, "SEURAAVA")

    assert new_text == "{{Alaboksi|edellinen=|seuraava=SEURAAVA}}", "Didn't remove edellinen"

    new_text = update_alaboksi(text, "EDELLINEN", None)

    assert new_text == "{{Alaboksi|edellinen=EDELLINEN|seuraava=}}", "Didn't remove seuraava"



def clone_edellinen_seuraava_template(wikitext):
    wikicode = mwparserfromhell.parse(wikitext)
    ed_o, seur_o = get_ed_seur_from_otsikko_template(wikicode)
    ed_a, seur_a = get_ed_seur_from_alaboksi_template(wikicode)

    if (ed_o, seur_o) == (None, None):
        return str(wikitext)

    if (ed_a, seur_a) == (ed_o, seur_o):
        return str(wikitext)

    if (ed_a, seur_a) == (None, None):
        new_text = add_before_categories(wikitext, """

{{Alaboksi
 | edellinen = %s
 | seuraava  = %s
}}

""" % (ed_o, seur_o))

    else:
        new_text = update_alaboksi(wikitext, ed_o, seur_o)

    return str(new_text)


def test__clone_edellinen_seuraava_template__adds_template():
    wikitext = """
{{Otsikko
|edellinen=Sivu 1
|seuraava=Sivu 2
}}

Some content.
"""
    wikicode = mwparserfromhell.parse(wikitext)
    result = clone_edellinen_seuraava_template(wikicode)

    print("REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEESSSSSSSSSSSSSS")
    print(result)
    assert result != wikitext, "Did not change"
    assert result.rstrip('\n') == """
{{Otsikko
|edellinen=Sivu 1
|seuraava=Sivu 2
}}

Some content.

{{Alaboksi
|edellinen=Sivu 1
|seuraava=Sivu 2
}}
""".rstrip('\n')

def test__clone_edellinen_seuraava_template__doesnt_add_if_already_there():
    wikitext = """
{{Otsikko
|edellinen=Sivu 1
|seuraava=Sivu 2
}}

Some content.

{{Alaboksi
|edellinen=Sivu 1
|seuraava=Sivu 2
}}
"""
    wikicode = mwparserfromhell.parse(wikitext)

    result = clone_edellinen_seuraava_template(wikicode)

    assert result.rstrip('\n') == wikitext.rstrip('\n'), "Changed, but shouldn't"



def test__clone_edellinen_seuraava_template__updates_alaboksi_if_out_of_sync():
    wikitext = """
{{Otsikko
|edellinen=Sivu 1
|seuraava=Sivu 2
}}

Some content.

{{Alaboksi
|edellinen=Sivu 2
|seuraava=Sivu 3
}}
"""
    wikicode = mwparserfromhell.parse(wikitext)

    result = clone_edellinen_seuraava_template(wikicode)

    assert result.rstrip('\n') == """
{{Otsikko
|edellinen=Sivu 1
|seuraava=Sivu 2
}}

Some content.

{{Alaboksi
|edellinen=Sivu 1
|seuraava=Sivu 2
}}
""".rstrip('\n')
