from hunsbotlive.script import IScript

class Script(IScript):

    def __init__(self, args):
        self.schedule = IScript.get_schedule(__file__)
        self.min_delay = IScript.get_delay(self.schedule)

    def __enter__(self, env=None):
        return self

    def __exit__(self, type, value, traceback, env=None):
        pass

    @property
    def name(self):
        return __name__

    def edit(self, page):
        print("Handling page:", page)
        return ""


def init(args):
    return Script(args)
