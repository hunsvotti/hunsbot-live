#!/usr/bin/env python3

import logging
import sys
import sqlite3

logger = logging.getLogger(__name__)

class DBConn:

    def __init__(self, filename):
        self.filename = filename

    def __setitem__(self, title, key):
        cursor = self.conn.cursor()

        try:
            cursor.execute('INSERT INTO katsokeys (title, key) VALUES (?, ?)', (title, key))
        except sqlite3.IntegrityError as ie:
            print(ie)


    def try_create_table(self):
        cursor = self.conn.cursor()

        try:
            cursor.execute('''CREATE TABLE IF NOT EXISTS katsokeys (title TEXT, key TEXT, PRIMARY KEY (title))''')
        except sqlite3.OperationalError as oe:
            print(oe)

        self.conn.commit()


    def __getitem__(self, title):
        cursor = self.conn.cursor()

        try:
            rows = cursor.execute('''
            SELECT title
            FROM katsokeys
            WHERE key IN (
                SELECT key
                FROM katsokeys
                WHERE title = ?
            )
            ORDER BY title
            ''', (title,))
        except Exception as e:
            raise e

        data = rows.fetchall()

        cur_key = None
        cur_list = []
        for row in data:
            if row[0] != title:
                cur_list.append(row[0])

        return cur_list

    def __delitem__(self, title):
        cursor = self.conn.cursor()

        try:
            rows = cursor.execute('''
            DELETE FROM katsokeys
            WHERE title = ?
            ''', (title,))
        except Exception as e:
            raise e

    def __enter__(self):
        self.conn = sqlite3.connect(self.filename)
        self.try_create_table()

        return self

    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()
