#!/usr/bin/env python3

import logging
import sys
import sqlite3

logger = logging.getLogger(__name__)

class DBConn:

    def __init__(self, tablename, filename):
        if any(map(lambda ch: not ch.isalnum(), tablename)):
            raise Exception(f"Non-alphanumeric characters not allowed: {tablename}")
        self.tablename = tablename + "_history"
        self.filename = filename

    def __setitem__(self, title, date_done):
        cursor = self.conn.cursor()

        try:
            cursor.execute(f'INSERT INTO {self.tablename} (title, date_done) VALUES (?, ?)', (title, date_done))
        except sqlite3.IntegrityError as ie:
            print(ie)


    def try_create_table(self):
        cursor = self.conn.cursor()

        try:
            cursor.execute(f'''CREATE TABLE IF NOT EXISTS {self.tablename} (title TEXT, date_done TEXT, PRIMARY KEY (title))''')
        except sqlite3.OperationalError as oe:
            print(oe)

        self.conn.commit()


    def __getitem__(self, title):
        cursor = self.conn.cursor()
        rows = cursor.execute(f'''
            SELECT *
            FROM {self.tablename}
            WHERE title = ?
        ''', (title,))

        results = rows.fetchall()
        if len(results) == 0:
            return None

        _, date = results[0]
        return date


    def __contains__(self, title):
        return self[title] is not None


    def __delitem__(self, title):
        cursor = self.conn.cursor()

        rows = cursor.execute(f'''
        DELETE FROM {self.tablename}
        WHERE title = ?
        ''', (title,))


    def clear_date(self, date_done):
        cursor = self.conn.cursor()

        rows = cursor.execute(f'''
        DELETE FROM {self.tablename}
        WHERE date_done = ?
        ''', (date_done,))


    def __enter__(self):
        self.conn = sqlite3.connect(self.filename)
        self.try_create_table()

        return self

    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()

    def items(self):
        cursor = self.conn.cursor()

        try:
            rows = cursor.execute(f'''
            SELECT *
            FROM {self.tablename}
            ''')
        except Exception as e:
            raise e

        for row in rows.fetchall():
            yield (row[0], row[1])

        return None
