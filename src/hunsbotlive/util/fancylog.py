import atexit
from datetime import datetime
import re
import time
import sqlite3

MAX_ROWS = 500

class FancyLog:
    def __init__(self, filename):
        self.filename = filename
        self.conn = sqlite3.connect(self.filename)
        atexit.register(self.__exit)

    def __insert_entry(self, status, script, title, descr, test=False):
        cursor = self.conn.cursor()
        date = datetime.now().replace(microsecond=0)
        cursor.execute(f'''
        INSERT INTO log_entry (date, script, title, descr, status, test)
        VALUES (?, ?, ?, ?, ?, ?)''', (date, script, title, descr, status, test))

    def success(self, script, title, comment, test=False):
        self.__insert_entry(2, script, title, comment, test)

    def meeh(self, script, title, comment, test=False):
        self.__insert_entry(1, script, title, comment, test)

    def error(self, script, title, comment, test=False):
        self.__insert_entry(0, script, title, comment, test)

    def __exit(self):
        self.conn.commit()
        self.conn.close()

    def save(self):
        self.conn.commit()


if __name__ == '__main__':
    fancylog = FancyLog('/home/antti/Projektit/legendary-journey/src/db.sqlite3')

    for i in range(0, 10):
        if i % 3 == 0:
            fancylog.success('test', i, 'added %d' % (i,))
        elif i % 3 == 1:
            fancylog.meeh('test', i, 'nochange %d' % (i,))
        else:
            fancylog.error('test', i, "error %d" % (i,))
        time.sleep(1)
