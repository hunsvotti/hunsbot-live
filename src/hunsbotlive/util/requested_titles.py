#!/usr/bin/env python3

import atexit
import logging
import sys
import sqlite3

import pywikibot
from pywikibot import pagegenerators

from ..config import LOGDB_PATH

logger = logging.getLogger(__name__)

class RequestedTitlesPageGenerator:

    def __init__(self, site=None):
        self.conn = sqlite3.connect(LOGDB_PATH)
        atexit.register(self.__finish)
        cursor = self.conn.cursor()
        rows = cursor.execute('''
            SELECT title
            FROM requests_api_request
            LIMIT 100
        ''')

        titles = [ row[0] for row in rows.fetchall() ]
        self.generator = pagegenerators.PagesFromTitlesGenerator(
            titles,
            site=site
        )

        print("Requests:", titles)

        cursor.execute('''
            DELETE
            FROM requests_api_request
            WHERE id in (
                SELECT id
                FROM requests_api_request
                LIMIT 100
            )
        ''')

        self.conn.commit()
        self.conn.close()

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.generator)

    def __finish(self):
        # Here we add all the rest of the titles back in case we ended abruptly.
        # We can't keep the db open all the time to update as we go, because
        # logging writes to the same db.

        self.conn = sqlite3.connect(LOGDB_PATH)
        cursor = self.conn.cursor()

        for page in self.generator:
            cursor.execute('''
                INSERT INTO requests_api_request
                (title, date_added)
                VALUES
                (?, '0000-00-00')
            ''', (page.title(),))

        self.conn.commit()
        self.conn.close()



if __name__ == "__main__":
    conn = sqlite3.connect(LOGDB_PATH)
    cursor = conn.cursor()

    for title in ['a', 'b', 'c', 'd', 'e']:
        cursor.execute(f'''
        INSERT INTO requests_api_request
        (title, date_added)
        VALUES (?, "2002-8-09")
        ''', (title))

    count = 3

    for page in RequestedTitlesPageGenerator():
        print(page.title(), page.text)
        count -= 1
