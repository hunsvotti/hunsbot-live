from datetime import timedelta
import re

import mwparserfromhell
import pywikibot


def unique(items):
    return list(set(items))

def link_list(items):
    assert len(items) > 0, "Empty list"

    return '[[' + (']], [['.join(items)) + ']]'

def uppercasefirst(s):
    return s[0].upper() + s[1:]


def is_all_conjugation(section):
    text = re.sub('(^|\n)[^#][^\n]*', '', section).strip()
    definition_lines = text.split("\n")
    non_taivm_lines = [x for x in filter(lambda line: not re.search('(taivm|taivutusmuoto)', line), definition_lines)]

    if len([x for x in non_taivm_lines]) == 0:
        return True

    return False


def is_redirect(wikitext):
    return (wikitext.find("#OHJAUS") > -1 or wikitext.find("#REDIRECT") > -1
            or wikitext.find("#ohjaus") > -1 or wikitext.find("#redirect") > -1
            or wikitext.find("#Ohjaus") > -1 or wikitext.find("#Redirect") > -1)


def test__is_redirect__finds_redirects():
    assert is_redirect('\n#OHJAUS [[sivu]]\n'), "Couldn't detect redirect"
    assert is_redirect('\n#Ohjaus [[sivu]]\n'), "Couldn't detect redirect"
    assert is_redirect('\n#ohjaus [[sivu]]\n'), "Couldn't detect redirect"
    assert is_redirect('\n#REDIRECT [[sivu]]\n'), "Couldn't detect redirect"
    assert is_redirect('\n#Redirect [[sivu]]\n'), "Couldn't detect redirect"
    assert is_redirect('\n#redirect [[sivu]]\n'), "Couldn't detect redirect"


def is_ignorable(wikitext):
    return (has_template(wikitext, "roskaa") or has_template(wikitext, 'Roskaa') \
            or has_template(wikitext, "pikapoisto") or has_template(wikitext, 'Pikapoisto') \
            or has_template(wikitext, "poistettava") or has_template(wikitext, 'Poistettava') \
            or has_template(wikitext, "tarkistettava") or has_template(wikitext, 'Tarkistettava'))


def test__is_ignorable():
    assert is_ignorable("{{Roskaa}}"), "Couldn't detect ignorable"
    assert is_ignorable("{{roskaa}}"), "Couldn't detect ignorable"
    assert is_ignorable("{{Pikapoisto}}"), "Couldn't detect ignorable"

    assert not is_ignorable("tämä on roskaa"), "Falsly detected ignorable"
    assert not is_ignorable("pikapoiston paikka"), "Falsly detected ignorable"



def has_template(wikitext, template_name):
    wikicode = mwparserfromhell.parse(wikitext)
    for template in wikicode.filter_templates():
        if template.name.strip() == template_name:
            return True

    return False


def test__has_template():
    assert has_template("{{Roskaa}}", "Roskaa"), "Couldn't detect template"
    assert has_template("{{Roskaa\n}}", "Roskaa"), "Couldn't detect template"
    assert has_template("{{\nRoskaa\n}}", "Roskaa"), "Couldn't detect template"
    assert has_template("\n{{Roskaa}}", "Roskaa"), "Couldn't detect template"
    assert has_template("{{Roskaa}}\n==Dumdi==", "Roskaa"), "Couldn't detect template"

    assert not has_template("\n{{{Roskaa}}}\n==Dumdi==", "Roskaa"), "Template misdetection"
    assert not has_template("{{RoskaaEri}}", "Roskaa"), "Template misdetection"
    assert not has_template("{{EriRoskaa}}", "Roskaa"), "Template misdetection"


def is_too_new(page, delay: timedelta):
    repo = pywikibot.Site().data_repository()
    if page.latest_revision.timestamp > repo.server_time() - delay:
        return True
    return False



def get_suomi_section_text(wikitext):
    suomi = re.search(r"(^|\n)== *Suomi *==\n", wikitext)
    if not suomi:
        return None

    start = suomi.start(0) + 10
    kans = re.search(r"(^|\n)== *Kansainvälinen *==\n", wikitext)
    muu = re.search(r"(^|\n)== *[^=\n]* *==\n", wikitext[start:])


    if muu:
        end = start + muu.start(0)
    else:
        end = len(wikitext)

    if kans:
        start = suomi.start(0)
    else:
        start = 0

    return wikitext[start:end]
