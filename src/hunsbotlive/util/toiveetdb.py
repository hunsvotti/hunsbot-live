#!/usr/bin/env python3

import logging
import sys
import sqlite3

logger = logging.getLogger(__name__)

class DBConn:

    def __init__(self, filename):
        self.filename = filename

    def __setitem__(self, request_title, index_page):
        cursor = self.conn.cursor()

        try:
            cursor.execute('INSERT INTO article_request (request_title, index_page) VALUES (?, ?)', (request_title, index_page))
        except sqlite3.IntegrityError as ie:
            print(ie)


    def try_create_table(self):
        cursor = self.conn.cursor()

        try:
            cursor.execute('''CREATE TABLE IF NOT EXISTS article_request (request_title TEXT, index_page TEXT, PRIMARY KEY (index_page, request_title))''')
        except sqlite3.OperationalError as oe:
            print(oe)

        self.conn.commit()


    def __getitem__(self, request_title):
        cursor = self.conn.cursor()

        try:
            rows = cursor.execute('''
            SELECT index_page
            FROM article_request
            WHERE request_title = ?
            ''', (request_title,))
        except Exception as e:
            raise e

        indices = []
        for row in rows.fetchall():
            indices.append(row[0])

        return indices


    def pages_of_index(self, index_title):
        cursor = self.conn.cursor()

        try:
            rows = cursor.execute('''
            SELECT request_title
            FROM article_request
            WHERE index_page = ?
            ''', (index_title,))
        except Exception as e:
            raise e

        titles = []
        for row in rows.fetchall():
            titles.append(row[0])

        return titles

    def index_pages(self):
        cursor = self.conn.cursor()

        try:
            rows = cursor.execute('''
            SELECT DISTINCT index_page
            FROM article_request
            ''')
        except Exception as e:
            raise e

        titles = []
        for row in rows.fetchall():
            titles.append(row[0])

        return titles


    def request_titles(self, index):
        cursor = self.conn.cursor()

        if index:
            try:
                rows = cursor.execute('''
                SELECT DISTINCT request_title
                FROM article_request
                WHERE index_page = ?
                ''', (index,))
            except Exception as e:
                raise e
        else:
            try:
                rows = cursor.execute('''
                SELECT request_title
                FROM article_request
                ''')
            except Exception as e:
                raise e

        titles = []
        for row in rows.fetchall():
            titles.append(row[0])

        return titles


    def __delitem__(self, request_title):
        cursor = self.conn.cursor()

        rows = cursor.execute('''
        DELETE FROM article_request
        WHERE request_title = ?
        ''', (request_title,))


    def delete(self, index_page, request_title):
        cursor = self.conn.cursor()

        rows = cursor.execute('''
        DELETE FROM article_request
        WHERE request_title = ? AND index_page = ?
        ''', (request_title, index_page))



    def clear_index(self, index_page):
        cursor = self.conn.cursor()

        rows = cursor.execute('''
        DELETE FROM article_request
        WHERE index_page = ?
        ''', (index_page,))


    def __enter__(self):
        self.conn = sqlite3.connect(self.filename)
        self.try_create_table()

        return self

    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()
