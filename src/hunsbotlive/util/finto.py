#!/usr/bin/env python3

import requests
import time




class Finto:
    def __init__(self, vocab, lang='fi'):
        self.vocab = vocab
        self.lang = lang
        self.cache = {}

    def __getitem__(self, item):
        if item not in self.cache:
            self.cache[item] = self.__finto_get(item)

        return self.cache[item]

    def __finto_get(self, item):
        response = requests.get(
            'http://api.finto.fi/rest/v1/search',
            params={
                'vocab': self.vocab,
                'lang': self.lang,
                'query': item
            }
        )

        json_response = response.json()

        if 'results' in json_response:
            return json_response['results']

        return None
