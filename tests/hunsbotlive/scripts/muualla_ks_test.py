import pytest
from dataclasses import dataclass

from hunsbotlive.scripts.muualla_ks import Script



@pytest.fixture
def Settings():
    class Settings:
        test: bool = False
        delay: int = 3
        tags: list = []
    return Settings


def test__hello():
    print("hello")



def test__hello():
    from hunsbotlive.scripts.muualla_ks.muualla_ks import DB_FILE
    assert DB_FILE.startswith("/home/ana/Projektit/hunsbot-live-suite/data")


def test__inserter():
        from hunsbotlive.scripts.muualla_ks.inserter import score_sect



def test__Script__opens_db(Settings):
    with Script(Settings()) as s:
        assert s.env['muualla-ks-db'] != None

def test__Script__returns_name(Settings):
    with Script(Settings()) as s:
        assert s.name == 'hunsbotlive.scripts.muualla_ks.muualla_ks'

def test__Script__can_check_existence_of_item_in_db(Settings):
    with Script(Settings()) as s:
        db = s.env['muualla-ks-db']
        assert 'testi' in db

def test__Script__can_check_non_existence_of_item_in_db(Settings):
    with Script(Settings()) as s:
        db = s.env['muualla-ks-db']
        assert 'jiöxkjid' not in db
