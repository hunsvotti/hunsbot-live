import pytest

from hunsbotlive.script import Abort
from hunsbotlive.scripts.muualla_ks.inserter import add_template



def test__add_template__to_empty_section():
    orig = ""

    result = add_template(orig)
    expected = """\
*{{muualla-ks}}
"""

    assert result == expected


def test__add_template__to_non_empty_section():
    orig = """\
* another
"""

    result = add_template(orig)
    expected = """\
* another
*{{muualla-ks}}
"""

    assert result == expected



def test__add_template__adding_to_section_with_existing_template_aborts():
    orig = """
*{{muualla-ks}}
"""

    with pytest.raises(Abort):
        result = add_template(orig)


from hunsbotlive.scripts.muualla_ks.inserter import add_template_to_page


def test__add_template_to_page__adding_to_page_without_section_adds_section():
    orig = """\
==Suomi==
===Substantiivi===
{{fi-subs}}

"""

    result = add_template_to_page(orig)
    expected = """\
==Suomi==
===Substantiivi===
{{fi-subs}}

====Aiheesta muualla====
*{{muualla-ks}}
"""

    assert result == expected


def test__add_template_to_page__adding_to_page_with_section_appends_section():
    orig = """\
==Suomi==
===Substantiivi===
{{fi-subs}}

====Aiheesta muualla====
* toinen
"""

    result = add_template_to_page(orig)
    expected = """\
==Suomi==
===Substantiivi===
{{fi-subs}}

====Aiheesta muualla====
* toinen
*{{muualla-ks}}
"""

    assert result == expected



def test__add_template_to_page__adding_to_page_that_already_has_template_aborts():
    orig = """\
==Suomi==
===Substantiivi===
{{fi-subs}}

====Aiheesta muualla====
*{{muualla-ks}}
"""

    with pytest.raises(Abort):
        result = add_template_to_page(orig)
