import pytest

from hunsbotlive.state import State


def test__State():
    state = State("test")
    end_time = get_now()
    state.set_end_time(end_time)

    state.save()
    with open(state.state_file, 'r') as fp:
        s = json.load(fp)
        assert s['time'] == end_time.isoformat().replace('+00:00', 'Z')
